import toastr from 'toastr'
import 'bootstrap-daterangepicker/daterangepicker.js'
import $ from 'jquery'
var moment = require('moment')
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
var Chart = require('chart.js')

'use strict'

const AppInt = {
  setCheck: (elm, flag) => {
    for (let index = 0; index < elm.length; index++) {
      let element = elm[index];
      element.checked = flag
    }
  },
  getCheck: (elm) => {
    return elm.checked
  },
  setBulkMenu: (flag) => {
    let bulkMenu = document.querySelector('.bulk-menu')
    if (flag) bulkMenu.classList.remove('d-none')
    else bulkMenu.classList.add('d-none')
  },
  setHeight: (target, iFrame) => {
    const ptop = (32*2)+58+40
    let targetHeight = document.querySelector(target).offsetHeight
    let iF = document.getElementById(iFrame)
    //let doc = iF.contentDocument ? iF.contentDocument : iF.contentWindow.document

    iF.style.visibility = 'hidden'
    iF.style.height = '300px'
    iF.style.height = (targetHeight-ptop)+'px'
    iF.style.visibility = 'visible'
  },
  setId: (length) => {
    let result           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  },
}

/** 
 * Main Handle
 * @Author: @ekoRv
 * @Date: 2019-12-03 17:14:08 
 * @Desc: App declared on Head
 */
App = {
  /** 
   * Init App
   * @Date: 2019-12-06 10:33:52 
   */  
  init: function() {
    this.datePicker()
    this.CKE()
    this.setId()
  },
  clickSelectFile: function(dis) {
    let idrow = dis.getAttribute('data-id')
    $('.modal-body').load(base_url+"settings/loadfilemanager/"+idrow)
  },
  formatType: function(target, dis = false) {
    let idrow = document.getElementById('row-'+dis.getAttribute('data-id'))
    let boxF = idrow.querySelector('.box-format')
    let selectF = idrow.querySelector('.select-format')
    let boxItemF = idrow.querySelectorAll('.box-item-format')
    let typeValue = idrow.querySelector('#type-'+dis.getAttribute('data-id'))

    console.log(typeValue)
    boxF.classList.remove('d-none')
    selectF.classList.add('d-none')
    Array.from(boxItemF).forEach(element => {
      element.classList.add('d-none')
    });
    idrow.querySelector('.format-'+target).classList.remove('d-none')
    typeValue.value = target
  },
  backFormat: function(dis) {
    let idrow = document.getElementById('row-'+dis.getAttribute('data-id'))
    let boxF = idrow.querySelector('.box-format')
    let selectF = idrow.querySelector('.select-format')
    boxF.classList.add('d-none')
    selectF.classList.remove('d-none')
  },
  clickCloned: function(id) {
    let button = document.getElementById(id)
    let idrow = button.getAttribute('data-id')

    document.getElementById('row-'+idrow).remove()
  },
  setId: () => {
    let metaList = document.querySelectorAll('.meta-list-wrapper > .meta-item')
    Array.from(metaList).forEach((element, i) => {
      let button = element.querySelector('.meta-item-button')
      let backToSelectFormat = element.getElementsByClassName('back-to-select-format')
      let buttonSelectFile = element.querySelector('.btn-select-file')
      let typeValue = element.querySelector('.setting-type-value')
      let imgSelectFile = element.querySelector('.setting-image')
      let idrow = AppInt.setId(6)

      let formRadio = element.getElementsByClassName('btn-setting-type')
      if (formRadio && formRadio.length > 0) {
        Array.from(formRadio).forEach((elm) => {
          elm.setAttribute('data-id', idrow)
        })
      }

      if (backToSelectFormat && backToSelectFormat.length > 0) {
        Array.from(backToSelectFormat).forEach((elm) => {
          elm.setAttribute('data-id', idrow)
        })
      }

      element.setAttribute('id', 'row-'+idrow)
      button.setAttribute('id', 'button-'+idrow)
      button.setAttribute('data-id', idrow)
      buttonSelectFile.setAttribute('data-id', idrow)
      imgSelectFile.setAttribute('id', 'setting-image-'+idrow)
      typeValue.setAttribute('id', 'type-'+idrow)
    });
  },
  cloneElement: function(original, target, ext = false) {
    let ori = document.getElementById(original)
    let cln = ori.cloneNode(true)
    let button = cln.querySelector('.meta-item-button')
    let backToSelectFormat = cln.getElementsByClassName('back-to-select-format')
    let buttonSelectFile = cln.querySelector('.btn-select-file')
    let typeValue = cln.querySelector('.setting-type-value')
    let imgSelectFile = cln.querySelector('.setting-image')
    let trg = document.querySelector(target)
    let idrow = AppInt.setId(6)

    if(ext) {
      let formRadio = cln.getElementsByClassName('btn-setting-type')
      if (formRadio.length > 0) {
        Array.from(formRadio).forEach((elm) => {
          elm.setAttribute('data-id', idrow)
        })
      }

      if (backToSelectFormat && backToSelectFormat.length > 0) {
        Array.from(backToSelectFormat).forEach((elm) => {
          elm.setAttribute('data-id', idrow)
        })
      }

      buttonSelectFile.setAttribute('data-id', idrow)
      imgSelectFile.setAttribute('id', 'setting-image-'+idrow)
      typeValue.setAttribute('id', 'type-'+idrow)
    }

    cln.setAttribute('id', 'row-'+idrow)
    button.setAttribute('id', 'button-'+idrow)
    button.setAttribute('data-id', idrow)
    cln.classList.remove('d-none')
    
    trg.appendChild(cln)
  },
  getHeight: (target, element) => {
    AppInt.setHeight(target, element)
  },
  /** 
   * CKEditor 5 init
   * @Date: 2019-12-09 09:32:32 
   * @Required: class .editor
   */  
  CKE: function() {
    let ins = document.querySelectorAll( '.editor' )
    ins.forEach(element => {
      ClassicEditor.create( element )
        .then( editor => {
          console.log( editor )
        } )
        .catch( error => {
          console.error( error )
        } );
    });
  },
  /** 
   * daterangepicker
   * @Author: flydreame 
   * @Date: 2019-12-06 10:34:16 
   * @Desc:  
   */  
  datePicker: () => {
    // $('.daterangepicker').each((i, obj) => {
    //   console.log(obj)
    //   obj.daterangepicker()
    // })
    let dateInit = moment()
    let today = dateInit.format('YYYY-MM-DD')
    let nextMonth = dateInit.add(1, 'month').format('YYYY-MM-DD')

    let dateInput = '.datepicker';
    let dateConfiguration = {
        locale: {
          format: 'YYYY-MM-DD'
        },
        opens: 'right',
        autoApply: true,
        startDate: today,
        endDate: nextMonth
    };

    $(dateInput).daterangepicker(dateConfiguration);
    $(dateInput).on('apply.daterangepicker', function(ev, picker) {
      console.log(picker.startDate.format('YYYY-MM-DD'));
      console.log(picker.endDate.format('YYYY-MM-DD'));
    });
  },
  /** 
   * @Author: @ekoRv 
   * @Date: 2019-12-03 17:07:31 
   * @Desc: toggle individual checkbox untuk menampilkan bulk menu
   * @Required: input:checkbox dengan format id="id-{i}"
   */  
  toggleCheck: (i) => {
    let elm = document.getElementById('check-'+i)
    AppInt.setBulkMenu(AppInt.getCheck(elm))
  },
  /** 
   * @Author: @ekoRv 
   * @Date: 2019-12-03 17:07:31 
   * @Desc: toggle All checkbox untuk menampilkan set check/uncheck checkbox && bulk menu
   * @Required: parent = input:checkbox dengan format id="check_all"
   *            childs = input:checkbox dengan format class="bulk-checkbox"
   */  
  toggleCheckAll: () => {
    let checkAll = document.querySelector('#check_all')
    let checkChilds = document.getElementsByClassName('bulk-checkbox')

    AppInt.setCheck(checkChilds, AppInt.getCheck(checkAll))
    AppInt.setBulkMenu(AppInt.getCheck(checkAll))
  },
  /** 
   * @Author: @ekoRv
   * @Date: 2019-12-04 10:25:24 
   * @Desc: toggle invidual element
   * @required: attribute data-target="{#}style"
   */  
  toggleElement: (elmt) => {
            let elm = elmt
                elm = document.querySelector(elm)
    let targetClass = document.getElementsByClassName('toggle-element-target')

    for (let index = 0; index < targetClass.length; index++) {
      let element = targetClass[index];
      
      element.classList.add('d-none')
    }

    elm.classList.remove('d-none')
  },
  /** 
   * @Author: @ekoRv 
   * @Date: 2019-12-03 17:07:31 
   * @Desc: copy paste exact value dari input 1 ke input lain saat onblur && mengubah value menjadi slug
   * @Required: input 1 dengan format class="blur-slug"
   *            input 1 dengan data attribute data-slug-form="{#/.}style"
   *            input 2 dengan format {id/class}="{data-slug-form value input 1}"
   */  
  blurSlug: function() {
    let fromBlur = document.querySelector('.blur-slug')
    let slugBlur = fromBlur.getAttribute('data-slug-form')
    let toHere = document.querySelector(slugBlur)

    return toHere.value = this.slugify(fromBlur.value)
  },
  /** 
   * @Author: @ekoRv
   * @Date: 2019-12-05 14:44:22 
   * @Desc: convert field text to slug format
   * @Required: input dengan format class="blur-slug-self"
   */
  blurSlugSelf: function() {
    let formBlur = document.querySelector('.blur-slug-self')

    return formBlur.value = this.slugify(formBlur.value)
  },
  slugify: (string) => {
    let a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
    let b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
    let p = new RegExp(a.split('').join('|'), 'g')
  
    return string.toString().toLowerCase()
      .replace(/\s+/g, '-') // Replace spaces with -
      .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
      .replace(/&/g, '-and-') // Replace & with 'and'
      .replace(/[^\w\-]+/g, '') // Remove all non-word characters
      .replace(/\-\-+/g, '-') // Replace multiple - with single -
      .replace(/^-+/, '') // Trim - from start of text
      .replace(/-+$/, '') // Trim - from end of text
  }
}

App.init();
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": true,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": true,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

/** 
 * @Author: @ekoRv
 * @Date: 2019-12-03 17:14:08 
 * @Desc: Alert declared on Head
 */
if (Alert.success.is_valid) toastr.success(Alert.success.message, 'Success!')
if (Alert.alert.is_valid) toastr.alert(Alert.alert.message, 'Alert!')
if (Alert.danger.is_valid) toastr.danger(Alert.danger.message, 'Danger!')
if (Alert.info.is_valid) toastr.info(Alert.info.message, 'Info!')

/** 
 * @Author: @ekoRv
 * @Date: 2019-12-11 13:54:33 
 * @Desc: Google Analytics Embedded API
 */
gapi.analytics.ready(function() {
  fetch(base_url+"googletest")
  .then((response) => {
    return response.json()
  })
  .then((responsedata) => {
    gapi.analytics.auth.authorize({
      container: 'embed-api-auth-container',
      //clientid: '798605422039-jjpog62jnatcvfvi4h3lugqgvtqanjev.apps.googleusercontent.com',
      clientid: responsedata.client_id,
      serverAuth: {
        access_token: responsedata.access_token
      }
    });

    // active user
    let activeUsers = new gapi.analytics.ext.ActiveUsers({
      container: 'active-users-container',
      pollingInterval: 5
    });

    // css animation user come & go
    activeUsers.once('success', function() {
      let element = this.container.firstChild;
      let timeout;
  
      this.on('change', function(data) {
        let element = this.container.firstChild;
        let animationClass = data.delta > 0 ? 'is-increasing' : 'is-decreasing';
        element.className += (' ' + animationClass);
  
        clearTimeout(timeout);
        timeout = setTimeout(function() {
          element.className =
              element.className.replace(/ is-(increasing|decreasing)/g, '');
        }, 3000);
      });
    });

    // let dataChart = new gapi.analytics.googleCharts.DataChart({
    //   reportType: 'ga',
    //   query: {
    //     metrics: ['ga:sessions'],
    //     dimensions: 'ga:date',
    //     'start-date': '30daysAgo',
    //     'end-date': 'yesterday',
    //     'ids': data.ids
    //   },
    //   chart: {
    //     container: 'chart-container',
    //     type: 'LINE',
    //     options: {
    //       width: '100%'
    //     }
    //   }
    // });

    if (gapi.analytics.auth.isAuthorized()) {
      //dataChart.execute()
      // Start tracking active users for this view.

      let title = document.getElementById('view-name');
      title.textContent = data.property.name + ' (' + data.view.name + ')';

      let data = {
        account: '149873947',
        property: 'UA-149873947-1',
        view: '203730689',
        ids: responsedata.ids
      }
      activeUsers.set(data).execute();

      // Render all the of charts for this view.
      renderWeekOverWeekChart(data.ids);
      renderYearOverYearChart(data.ids);
      renderTopBrowsersChart(data.ids);
      renderTopCountriesChart(data.ids);
    }

    function renderWeekOverWeekChart(ids) {

      // Adjust `now` to experiment with different days, for testing only...
      let now = moment(); // .subtract(3, 'day');
  
      let thisWeek = query({
        'ids': ids,
        'dimensions': 'ga:date,ga:nthDay',
        'metrics': 'ga:sessions',
        'start-date': moment(now).subtract(1, 'day').day(0).format('YYYY-MM-DD'),
        'end-date': moment(now).format('YYYY-MM-DD')
      });
  
      let lastWeek = query({
        'ids': ids,
        'dimensions': 'ga:date,ga:nthDay',
        'metrics': 'ga:sessions',
        'start-date': moment(now).subtract(1, 'day').day(0).subtract(1, 'week')
            .format('YYYY-MM-DD'),
        'end-date': moment(now).subtract(1, 'day').day(6).subtract(1, 'week')
            .format('YYYY-MM-DD')
      });
  
      Promise.all([thisWeek, lastWeek]).then(function(results) {
  
        let data1 = results[0].rows.map(function(row) { return +row[2]; });
        let data2 = results[1].rows.map(function(row) { return +row[2]; });
        let labels = results[1].rows.map(function(row) { return +row[0]; });
  
        labels = labels.map(function(label) {
          return moment(label, 'YYYYMMDD').format('ddd');
        });
  
        let data = {
          type: 'line',
          data: {
            labels : labels,
            datasets : [
              {
                label: 'Last Week',
                fillColor : 'rgba(220,220,220,0.5)',
                strokeColor : 'rgba(220,220,220,1)',
                pointColor : 'rgba(220,220,220,1)',
                pointStrokeColor : '#fff',
                data : data2
              },
              {
                label: 'This Week',
                fillColor : 'rgba(151,187,205,0.5)',
                strokeColor : 'rgba(151,187,205,1)',
                pointColor : 'rgba(151,187,205,1)',
                pointStrokeColor : '#fff',
                data : data1
              }
            ]
          }
        };
  
        //new Chart(makeCanvas('chart-1-container')).Line(data);
        new Chart(makeCanvas('chart-1-container'), data);
        //generateLegend('legend-1-container', data.datasets);
      });
    }

    function renderYearOverYearChart(ids) {

      // Adjust `now` to experiment with different days, for testing only...
      let now = moment(); // .subtract(3, 'day');
  
      let thisYear = query({
        'ids': ids,
        'dimensions': 'ga:month,ga:nthMonth',
        'metrics': 'ga:users',
        'start-date': moment(now).date(1).month(0).format('YYYY-MM-DD'),
        'end-date': moment(now).format('YYYY-MM-DD')
      });
  
      let lastYear = query({
        'ids': ids,
        'dimensions': 'ga:month,ga:nthMonth',
        'metrics': 'ga:users',
        'start-date': moment(now).subtract(1, 'year').date(1).month(0)
            .format('YYYY-MM-DD'),
        'end-date': moment(now).date(1).month(0).subtract(1, 'day')
            .format('YYYY-MM-DD')
      });
  
      Promise.all([thisYear, lastYear]).then(function(results) {
        let data1 = results[0].rows.map(function(row) { return +row[2]; });
        let data2 = results[1].rows.map(function(row) { return +row[2]; });
        let labels = ['Jan','Feb','Mar','Apr','May','Jun',
                      'Jul','Aug','Sep','Oct','Nov','Dec'];
  
        // Ensure the data arrays are at least as long as the labels array.
        // Chart.js bar charts don't (yet) accept sparse datasets.
        for (let i = 0, len = labels.length; i < len; i++) {
          if (data1[i] === undefined) data1[i] = null;
          if (data2[i] === undefined) data2[i] = null;
        }
  
        let data = {
          type: 'bar',
          labels : labels,
          datasets : [
            {
              label: 'Last Year',
              fillColor : 'rgba(220,220,220,0.5)',
              strokeColor : 'rgba(220,220,220,1)',
              data : data2
            },
            {
              label: 'This Year',
              fillColor : 'rgba(151,187,205,0.5)',
              strokeColor : 'rgba(151,187,205,1)',
              data : data1
            }
          ]
        };
  
        new Chart(makeCanvas('chart-2-container'), data);
        generateLegend('legend-2-container', data.datasets);
      })
      .catch(function(err) {
        console.error(err.stack);
      });
    }

    function renderTopBrowsersChart(ids) {

      query({
        'ids': ids,
        'dimensions': 'ga:browser',
        'metrics': 'ga:pageviews',
        'sort': '-ga:pageviews',
        'max-results': 5
      })
      .then(function(response) {
        let labels = []
        let values = []
        response.rows.map(function(row) { 
          labels.push(row[0])
          values.push(row[1])
        });
  
        let colors = ['#4D5360','#949FB1','#D4CCC5','#E2EAE9','#F7464A'];
  
        let config = {
          type: 'doughnut',
          data: {
            labels: labels,
            datasets: [{
              data: values,
              backgroundColor: colors,
            }]
          }
        };
    
        new Chart(makeCanvas('chart-3-container'), config);
      });
    }

    function renderTopCountriesChart(ids) {
      query({
        'ids': ids,
        'dimensions': 'ga:country',
        'metrics': 'ga:sessions',
        'sort': '-ga:sessions',
        'max-results': 5
      })
      .then(function(response) {
        let labels = []
        let values = []
        response.rows.map(function(row) { 
          labels.push(row[0])
          values.push(row[1])
        });
  
        let colors = ['#4D5360','#949FB1','#D4CCC5','#E2EAE9','#F7464A'];
  
        let config = {
          type: 'doughnut',
          data: {
            labels: labels,
            datasets: [{
              data: values,
              backgroundColor: colors,
            }]
          }
        };
    
  
        new Chart(makeCanvas('chart-4-container'), config);
      });
    }
    function query(params) {
      return new Promise(function(resolve, reject) {
        let data = new gapi.analytics.report.Data({query: params});
        data.once('success', function(response) { resolve(response); })
            .once('error', function(response) { reject(response); })
            .execute();
      });
    }

    function makeCanvas(id) {
      let container = document.getElementById(id);
      let canvas = document.createElement('canvas');
      let ctx = canvas.getContext('2d');
  
      container.innerHTML = '';
      canvas.width = container.offsetWidth;
      canvas.height = container.offsetHeight;
      container.appendChild(canvas);
  
      return ctx;
    }

    function generateLegend(id, items) {
      let legend = document.getElementById(id);
      legend.innerHTML = items.map(function(item) {
        let color = item.color || item.fillColor;
        let label = item.label;
        return '<li><i style="background:' + color + '"></i>' +
            escapeHtml(label) + '</li>';
      }).join('');
    }

    // Set some global Chart.js defaults.
    Chart.defaults.global.animationSteps = 60;
    Chart.defaults.global.animationEasing = 'easeInOutQuart';
    Chart.defaults.global.responsive = true;
    Chart.defaults.global.maintainAspectRatio = false;

    function escapeHtml(str) {
      let div = document.createElement('div');
      div.appendChild(document.createTextNode(str));
      return div.innerHTML;
    }
  })
  .catch((error) => {
    console.log('fetch access token ', error)
  })
});