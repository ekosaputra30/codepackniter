const mix = require('laravel-mix');

mix.setPublicPath('client/public')
mix.js('client/resources/js/app.js', 'client/public/js');
mix.js('client/resources/secure/js/gapi-components.js', 'client/public/secure/js');
mix.js('client/resources/secure/js/app.js', 'client/public/secure/js');
mix.sass('client/resources/sass/app.scss', 'client/public/css/app.css');
mix.sass('client/resources/secure/scss/app.scss', 'client/public/secure/css');
mix.disableNotifications();