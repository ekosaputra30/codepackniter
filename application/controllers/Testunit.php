<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testunit extends CI_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->library('Unit_test');
  }
  

  public function calculate_discount($discountRate, $originalPrice)
  {
    $discount = $originalPrice * $discountRate / 100;
    return $originalPrice - $discount;    
  }

  public function  test_discount() {
    $test = $this->calculate_discount(50, 1000);
    $expect = 500;
    $name = 'menghitung diskon';

    $data = [
      'test' => $test,
      'expect' => $expect,
      'name' => $name
    ];

    return $this->load->view('unit_test', $data, FALSE);
    
  }

}

/* End of file Testunit.php */
