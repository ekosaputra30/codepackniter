<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require 'vendor/autoload.php';

class Googletest extends CI_Controller {
  protected $_CLIENTID = '798605422039-jjpog62jnatcvfvi4h3lugqgvtqanjev.apps.googleusercontent.com';
  protected $_SERVICE_ACCOUNT_NAME = 'analytics-api@xtoserpong-site.iam.gserviceaccount.com ';
  protected $_KEY;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->library('google');
    //$this->_KEY = base_url('xtoserpong-site-4d27e35da3ba.json');
    $this->_KEY = 'xtoserpong-site-4d27e35da3ba.json';
  }
  
  public function index()
  {
    $cre = $this->getJson();

    $this->google->setScopes(array('https://www.googleapis.com/auth/analytics')); 
    putenv('GOOGLE_APPLICATION_CREDENTIALS='.$this->_KEY);
    
    $this->google->setAuthConfig($this->_KEY);
    $this->google->useApplicationDefaultCredentials();
    $this->google->addScope(Google_Service_Drive::DRIVE);
    $this->google->setSubject($cre->client_email);
 
    if ($this->google->isAccessTokenExpired()) {
      $this->google->refreshTokenWithAssertion();
    }
 
    $arrayInfo = $this->google->getAccessToken();
 
    $accesstoken = $arrayInfo['access_token'];
    $return = $cre;
    $return->ids = 'ga:203730689';
    $return->access_token = $accesstoken;

    header('Content-Type: application/json');
    echo json_encode($return);
  }

  public function getJson() {
    $key = json_decode(file_get_contents($this->_KEY));

    return $key;
  }

}

/* End of file Googletest.php */
