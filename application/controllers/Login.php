<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->library('Auth');
  }

  public function index()
  {
    $data = [
      'page_view' => 'login/form',
      'page_data' => [
        'form_action'   => site_url('login/process'),
        'form_login_cb' => $this->get_fdata('form_login_cb')
      ]
    ];

    $this->_render_page($data);
  }

  public function process()
  {
    $this->load->model('rbac/User_model');
    $this->load->model('Privileged_model');
    
    $post = $this->input->post();
    $user = $this->User_model->getByEmail($post['email']);

    if ( $user->count == 1 ) {
      if ( password_verify($post['password'], $user->result['password']) == TRUE ) {
        $role = $this->Privileged_model->getRoleId($user->result['id']);
        $privileges = $this->auth->getRole($role['role_id']);
        
        $session = [
          'is_secure' => true,
          'email' => $post['email'],
          'role' => $role['role_name'],
          'permissions' => $privileges['roles']
        ];
        $this->session->set_userdata( 'cms_data', $session );
        return redirect(site_url('admin'));
      } else {
        $form_callback = 'kombinasi Kata sandi dan E-mail tidak sesuai';
  
        $this->set_fdata('form_login_cb', $form_callback);
        return redirect(site_url('login?err=wrong-combination'));  
      }
    } else {
      $form_callback = 'Alamat E-mail tidak terdaftar';

      $this->set_fdata('form_login_cb', $form_callback);
      return redirect(site_url('login?err=user-not-exist'));
    }  
  }
  
  public function set_fdata($name = false, $value = false) {
    return $this->session->set_flashdata($name, $value);
  }

  public function get_fdata($name) {
    return $this->session->flashdata($name);
  }

}

/* End of file Landing.php */
