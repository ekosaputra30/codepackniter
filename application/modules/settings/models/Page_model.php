<?php

class Page_model extends CI_Model
{
  private $_table = 'pages';

  public function get($id = false) {
    $return = new ArrayObject();
    $query = $this->db->get_where($this->_table, ['id' => $id]);

    $return->count  = $query->num_rows();
    $return->result = $query->row_array();

    return $return;  
  }

  public function get_all($sort = false, $ascdesc = false) {
    $return = new ArrayObject();

    if ($sort) $this->db->order_by('sort_num', $ascdesc);
    else $this->db->order_by('id', 'DESC');
    $query = $this->db->get($this->_table);
    
    $return->count  = $query->num_rows();
    $return->result = $query->result_array();

    return $return;
  }

  public function save($data) {
    if ($this->db->insert($this->_table, $data)) return $this->db->insert_id();

    else return false;
  }

  public function edit($id, $data) {
    if ($this->db->update($this->_table, $data, ['id' => $id])) return true;

    else return false;
  }
}
