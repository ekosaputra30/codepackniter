<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model {

  protected $_table = 'settings';

  public function get($id = false) {
    $return = new ArrayObject();
    $query = $this->db->get_where($this->_table, ['id' => $id]);

    $return->count  = $query->num_rows();
    $return->result = $query->row_array();

    return $return;  
  }

  public function get_all() {
    $return = new ArrayObject();

    $query = $this->db->get($this->_table);
    
    $return->count  = $query->num_rows();
    $return->result = $query->result_array();

    return $return;
  }

  public function save($data) {
    if ($this->db->insert($this->_table, $data)) return $this->db->insert_id();

    else return false;
  }

  public function edit($id, $data) {
    if ($this->db->update($this->_table, $data, ['id' => $id])) return true;

    else return false;
  }

  public function deleteAll() {
    return $this->db->truncate($this->_table);
  }

  public function delete($id) {
    if ($this->db->delete($this->_table, ['id' => $id])) return true;

    else return false;
  }

}

/* End of file Setting_model.php */
