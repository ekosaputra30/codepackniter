<?php

class Banner_model extends CI_Model
{
  private $_table = 'banners';
  private $_table_page = 'pages';
  private $_table_block = 'blocks';

  public function get($id = false) {
    $return = new ArrayObject();
    $this->db->select('banners.id AS banner_id, banners.*, blocks.id AS block_id, blocks.*, pages.page_name, pages.module_name, pages.slug');
    $this->db->from($this->_table);
    $this->db->join('blocks', 'blocks.id = banners.block');
    $this->db->join('pages', 'pages.id = blocks.page');
    $this->db->where('banners.id', $id);
    $query = $this->db->get();

    $return->count  = $query->num_rows();
    $return->result = $query->row_array();

    return $return;  
  }

  public function get_all($sort = false, $ascdesc = false) {
    $return = new ArrayObject();

    if ($sort) $this->db->order_by('banners.sort_num', $ascdesc);
    else $this->db->order_by('banners.id', 'DESC');
    $this->db->select('banners.id, blocks.*, pages.page_name, pages.module_name, pages.slug');
    $this->db->from($this->_table);
    $this->db->join('blocks', 'blocks.id = banners.block');
    $this->db->join('pages', 'pages.id = blocks.page');
    $query = $this->db->get();
    
    $return->count  = $query->num_rows();
    $return->result = $query->result_array();

    return $return;
  }

  public function save($data) {
    if ($this->db->insert($this->_table, $data)) return $this->db->insert_id();

    else return false;
  }

  public function edit($id, $data) {
    if ($this->db->update($this->_table, $data, ['id' => $id])) return true;

    else return false;
  }

  public function delete($id = 0) {
    $banner = $this->get($id);
    if ($banner->count > 0) {
      if ($this->db->delete($this->_table_block, ['id' => $banner->result['block_id']])) {
        $this->db->delete($this->_table, ['id' => $id]);

        return true;
      }
    }

    return false;
  }
}
