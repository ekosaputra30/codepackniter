<?php echo form_open($form_action) ?>
  <div class="card">
    <div class="card-header">
      <h4 class="mb-0">General Setting</h4>
    </div>
    <div class="card-body">
      <div class="meta-field meta-list-wrapper">
        <?php if($settings->count == 0): ?>
          <div class="form-group row meta-item">
            <div class="col-sm-4">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <button class="btn btn-danger btn-square meta-item-button" type="button" onclick="App.clickCloned(this.id)"><i class="cil-trash"></i></button>
                </div>
                <input type="text" name="setting-name[]" class="form-control">
              </div>
            </div>
            <div class="col-sm-8">
              <div class="select-format">
                select format : 
                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                  <label class="btn btn-light btn-square btn-setting-type active" onclick="App.formatType('TEXT', this)">
                    <input type="radio" name="setting-type-button[]" class="setting-type" value="TEXT"> TEXT
                  </label>
                  <label class="btn btn-light btn-square btn-setting-type" onclick="App.formatType('IMAGE', this)">
                    <input type="radio" name="setting-type-button[]" class="setting-type" value="IMAGE"> IMAGE
                  </label>
                  <input type="hidden" class="setting-type-value" name="setting-type[]" value="">
                </div>
              </div>
              <div class="box-format d-none">
                <div class="box-item-format format-TEXT d-none">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <button class="btn btn-default back-to-select-format" type="button" onclick="App.backFormat(this)"><i class="cil-arrow-left"></i></button>
                      <span class="input-group-text align-items-baseline">TEXT</span>
                    </div>
                    <textarea class="form-control" name="setting-text[]" placeholder=""></textarea>
                  </div>
                </div>
                <div class="box-item-format format-IMAGE d-none">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <button class="btn btn-default back-to-select-format" type="button" onclick="App.backFormat(this)"><i class="cil-arrow-left"></i></button>
                      <span class="input-group-text align-items-baseline">IMAGE</span>
                    </div>
                    <input type="text" class="form-control setting-image" name="setting-image[]" placeholder="">
                    <div class="input-group-prepend">
                      <a data-toggle="modal" href="javascript:;" data-target="#myModal" class="btn btn-square btn-primary btn-select-file" onclick="App.clickSelectFile(this)" type="button"><i class="cil-plus"></i> select file</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php else: ?>
          <?php foreach($settings->result as $s => $setting):?>
            <div class="form-group row meta-item">
              <input type="hidden" name="setting-id[]" value="<?php echo $setting['id'] ?>">
              <div class="col-sm-4">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <button class="btn btn-danger btn-square meta-item-button" type="button" onclick="App.clickCloned(this.id)"><i class="cil-trash"></i></button>
                  </div>
                  <input type="text" name="setting-name[]" class="form-control" value="<?php echo $setting['name'] ?>">
                </div>
              </div>
              <div class="col-sm-8">
                <div class="select-format <?php echo ($setting['type'] !== '') ? 'd-none' : false ?>">
                  select format : 
                  <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-light btn-square btn-setting-type <?php echo ($setting['type'] == 'TEXT') ? 'focus active' : false ?>" onclick="App.formatType('TEXT', this)">
                      <input type="radio" name="setting-type-button[]" class="setting-type" value="TEXT" <?php echo ($setting['type'] === 'TEXT') ? 'checked' : false ?>> TEXT
                    </label>
                    <label class="btn btn-light btn-square btn-setting-type <?php echo ($setting['type'] == 'IMAGE') ? 'focus active' : false ?>" onclick="App.formatType('IMAGE', this)">
                      <input type="radio" name="setting-type-button[]" class="setting-type" value="IMAGE" <?php echo ($setting['type'] === 'IMAGE') ? 'checked' : false ?>> IMAGE
                    </label>
                    <input type="hidden" class="setting-type-value" name="setting-type[]" vaule="<?php echo $setting['type'] ?>">
                  </div>
                </div>
                <div class="box-format <?php echo ($setting['type'] == '') ? 'd-none' : false ?>">
                  <div class="box-item-format format-TEXT <?php echo ($setting['type'] !== 'TEXT') ? 'd-none' : false ?>">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <button class="btn btn-default back-to-select-format" type="button" onclick="App.backFormat(this)"><i class="cil-arrow-left"></i></button>
                        <span class="input-group-text align-items-baseline">TEXT</span>
                      </div>
                      <textarea class="form-control" name="setting-text[]" placeholder=""><?php echo ($setting['type'] == 'TEXT') ? $setting['content'] : false ?></textarea>
                    </div>
                  </div>
                  <div class="box-item-format format-IMAGE <?php echo ($setting['type'] !== 'IMAGE') ? 'd-none' : false ?>">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <button class="btn btn-default back-to-select-format" type="button" onclick="App.backFormat(this)"><i class="cil-arrow-left"></i></button>
                        <span class="input-group-text align-items-baseline">IMAGE</span>
                      </div>
                      <input type="text" class="form-control setting-image" name="setting-image[]" placeholder="" value="<?php echo ($setting['type'] == 'IMAGE') ? $setting['content'] : false ?>">
                      <div class="input-group-prepend">
                        <a data-toggle="modal" href="javascript:;" data-target="#myModal" class="btn btn-square btn-primary btn-select-file" onclick="App.clickSelectFile(this)" type="button"><i class="cil-plus"></i> select file</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        <?php endif ?>
      </div>
      <button type="button" onclick="App.cloneElement('original-clone-field', '.meta-field', 'true')" class="btn btn-square btn-primary"><i class="cil-plus"></i> add setting</button>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <button class="btn btn-square btn-success" type="submit">Submit</button>
    </div>
  </div>
<?php echo form_close() ?>

<div class="input-group mb-3 meta-item d-none" id="original-clone-format">
  <div class="input-group-prepend">
    <button class="btn btn-danger btn-square meta-item-button" type="button" onclick="App.clickCloned(this.id)"><i class="cil-trash"></i></button>
  </div>
  <input type="text" class="form-control" name="setting-content[]" placeholder="">
</div>

<div class="form-group row d-none meta-item" id="original-clone-field">
  <div class="col-sm-4">
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <button class="btn btn-danger btn-square meta-item-button" type="button" onclick="App.clickCloned(this.id)"><i class="cil-trash"></i></button>
      </div>
      <input type="text" name="setting-name[]" class="form-control">
    </div>
  </div>
  <div class="col-sm-8">
    <div class="select-format">
      select format : 
      <div class="btn-group btn-group-toggle" data-toggle="buttons">
        <label class="btn btn-light btn-square btn-setting-type active" onclick="App.formatType('TEXT', this)">
          <input type="radio" name="setting-type-button[]" class="setting-type" value="TEXT"> TEXT
        </label>
        <label class="btn btn-light btn-square btn-setting-type" onclick="App.formatType('IMAGE', this)">
          <input type="radio" name="setting-type-button[]" class="setting-type" value="IMAGE"> IMAGE
        </label>
        <input type="hidden" class="setting-type-value" name="setting-type[]" value="">
      </div>
    </div>
    <div class="box-format d-none">
      <div class="box-item-format format-TEXT d-none">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <button class="btn btn-default back-to-select-format" type="button" onclick="App.backFormat(this)"><i class="cil-arrow-left"></i></button>
            <span class="input-group-text align-items-baseline">TEXT</span>
          </div>
          <textarea class="form-control" name="setting-text[]" placeholder=""></textarea>
        </div>
      </div>
      <div class="box-item-format format-IMAGE d-none">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <button class="btn btn-default back-to-select-format" type="button" onclick="App.backFormat(this)"><i class="cil-arrow-left"></i></button>
            <span class="input-group-text align-items-baseline">IMAGE</span>
          </div>
          <input type="text" class="form-control setting-image" name="setting-image[]" placeholder="">
          <div class="input-group-prepend">
            <a data-toggle="modal" href="javascript:;" data-target="#myModal" class="btn btn-square btn-primary btn-select-file" onclick="App.clickSelectFile(this)" type="button"><i class="cil-plus"></i> select file</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Select Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>