<?php echo form_open($form_action); ?>
<div class="card">
  <div class="card-header">
    <div class="btn-group bulk-menu d-none" role="group" aria-label="">
      <input type="hidden" name="bulk-method" value="delete">
      <button type="submit" onclick="return confirm('This action will delete selected items, are you sure?')" class="btn btn-danger"><i class="cil-trash"></i> delete selected items</button>
    </div>

    <a href="<?php echo module_url('create') ?>" class="btn btn-primary float-right">add page</a>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table width="100%" class="table table-condensed">
        <thead>
          <tr class="d-flex">
            <th class="col-sm-1 border-top-0">
              <div class="form-group">
                <div class="form-check checkbox">
                  <input type="checkbox" name="all_page" id="check_all" class="bulk-checkbox-all form-check-input" onclick="App.toggleCheckAll()">
                </div>
              </div>
            </th>
            <th class="col-sm-5 border-top-0">Page</th>
            <th class="col-sm-3 border-top-0">Module</th>
            <th class="col-sm-3 border-top-0"># &nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php if($pages->count == 0): ?>
            <tr>
              <td colspan="3">no record available</td>
            </tr>
          <?php else:?>
            <?php foreach($pages->result as $k => $page):?>
              <tr class="d-flex">
                <td class="col-sm-1">
                  <div class="form-group">
                    <div class="form-check checkbox">
                      <input type="checkbox" name="id[]" id="check-<?php echo $k+1 ?>" class="bulk-checkbox form-check-input" onclick="App.toggleCheck(<?php echo $k+1 ?>)" value="<?php echo $page['id'] ?>">
                    </div>
                  </div>
                </td>
                <td class="col-sm-5"><?php echo $page['page_name'] ?></td>
                <td class="col-sm-3"><?php echo $page['module_name'] ?></td>
                <td class="col-sm-3">
                  <div class="btn-group" role="group" aria-label="">
                    <a href="<?php echo module_url('edit/'.$page['id']) ?>" class="btn btn-info"><i class="cil-pencil"></i> edit</a>
                    <a href="<?php echo module_url('destroy/'.$page['id']) ?>" class="btn btn-danger" onclick="return confirm('are you sure?');"><i class="cil-trash"></i> delete</a>
                  </div>
                </td>
              </tr>
            <?php endforeach;?>
          <?php endif;?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<?php echo form_close(); ?>