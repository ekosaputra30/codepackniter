<?php echo form_open($form_action); ?>
<input type="hidden" name="id" value="<?php echo $id ?>">
<div class="row">
  <div class="col-6">
    <div class="card">
      <div class="card-header">
        <a href="<?php echo module_url() ?>" id="c-icon-details" class="btn btn-default float-left c-icon-x4">
          <i class="cil-arrow-left c-x128"></i>
        </a>
        <button type="submit" class="btn btn-success float-right">save</button>
      </div>
      <div class="card-body">
        <div class="form-group row">
          <label for="page_name" class="col-sm-4 col-form-label">Name</label>
          <div class="col-sm-8">
            <input type="text" name="page_name" id="page_name" class="form-control blur-slug" data-slug-form="#slug" placeholder="" value="<?php echo $result['page_name'] ?>" onblur="App.blurSlug()" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="slug" class="col-sm-4 col-form-label">Slug</label>
          <div class="col-sm-8">
            <input type="text" name="slug" id="slug" readonly class="form-control" placeholder="" value="<?php echo $result['slug'] ?>">
          </div>
        </div>
        <div class="form-group row">
          <label for="module_name" class="col-sm-4 col-form-label">Module / Controller</label>
          <div class="col-sm-8">
            <input type="text" name="module_name" id="module_name" class="form-control" placeholder="" required value="<?php echo $result['module_name'] ?>">
          </div>
        </div>
        <div class="form-group row">
          <label for="sort_num" class="col-sm-4 col-form-label">Order</label>
          <div class="col-sm-4">
            <input type="number" name="sort_num" id="sort_num" class="form-control" placeholder="" value="0" value="<?php echo $result['sort_num'] ?>">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-8 offset-sm-2">
            <button class="btn btn-success" type="submit">save</button>
            <a href="<?php echo module_url() ?>" class="btn btn-default">cancel</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>