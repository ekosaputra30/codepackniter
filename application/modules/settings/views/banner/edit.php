<?php echo form_open_multipart($form_action); ?>
<div class="row">
  <div class="col">
    <a href="<?php echo module_url() ?>" class="btn btn-default btn-square"><i class="cil-arrow-left"></i></a>
  </div>
</div>
<div class="d-flex flex-column-reverse flex-sm-row">
  <div class="col-12 col-sm-6">
    <div class="card">
      <div class="card-header">
        <h4>Meta</h4>
      </div>
      <div class="card-body">
        <div class="form-group row">
          <label for="name" class="col-sm-4 col-form-label">Block Name</label>
          <div class="col-sm-8">
            <input type="text" onblur="App.blurSlugSelf()" value="<?php echo $result['name'] ?>" name="name" id="name" class="form-control blur-slug-self" placeholder="" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="description" class="col-sm-4 col-form-label">Short Description</label>
          <div class="col-sm-8">
            <textarea name="description" id="description" class="form-control" placeholder=""><?php echo $result['description'] ?></textarea>
          </div>
        </div>
        <div class="form-group row">
          <label for="heading" class="col-sm-4 col-form-label">Heading Text</label>
          <div class="col-sm-8">
            <input type="text" value="<?php echo $result['heading'] ?>" name="heading" id="heading" class="form-control" placeholder="">
          </div>
        </div>
        <div class="form-group row">
          <label for="sub" class="col-sm-4 col-form-label">Sub Heading Text</label>
          <div class="col-sm-8">
            <input type="text" value="<?php echo $result['sub'] ?>" name="sub" id="sub" class="form-control" placeholder="">
          </div>
        </div>
        <div class="form-group row">
          <label for="content" class="col-sm-4 col-form-label">Content</label>
          <div class="col-sm-8">
            <textarea name="content" id="content" class="form-control" placeholder=""><?php echo $result['content'] ?></textarea>
          </div>
        </div>
        <div class="form-group row">
          <label for="style" class="col-sm-4 col-form-label">Custom Style</label>
          <div class="col-sm-8">
            <textarea name="style" id="style" class="form-control" placeholder=""><?php echo $result['style'] ?></textarea>
          </div>
        </div>
        <div class="form-group row">
          <label for="page" class="col-form-label col-sm-4">Page</label>
          <div class="col-sm-4">
            <select name="page" id="page" class="form-control">
              <option value="0">-choose page-</option>
              <?php foreach($pages->result as $k => $page):?>
                <option value="<?php echo $page['id'] ?>" <?php echo ($result['page'] == $page['id']) ? 'selected="selected"' : false ?>><?php echo $page['page_name'] ?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-sm-6">
    <div class="card">
      <div class="card-header">
        <h4>Image</h4>
      </div>
      <div class="card-body">
        <div class="form-group row">
          <label for="image" class="col-sm-4 col-form-label">Image</label>
          <div class="col-sm-8">
            <div class="input-group">
              <input type="text" value="<?php echo $result['image'] ?>" class="form-control" name="image" id="image" data-modal="myModal" placeholder="" aria-label="" required>
              <span class="input-group-btn">
                <a data-toggle="modal" href="javascript:;" data-target="#myModal" class="btn btn-square btn-primary" type="button">Select File</a>
              </span>
            </div>
          </div>
        </div>
        <div class="form-group row <?php echo ($result['image'] != '' ? false : 'd-none') ?>" id="preview_image_block">
          <label for="image" class="col-sm-4 col-form-label">Preview</label>
            <div class="col-sm-8">
              <img src="<?php echo $result['image'] ?>" id="preview_image" alt="" class="img-thumbnail">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <button type="submit" class="btn btn-success btn-square">Submit</button>
        <a href="<?php echo module_url(); ?>" class="btn btn-default btn-square">Cancel</a>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe width="100%" height="400" src="<?php echo base_url() ?>filemanager/dialog.php?type=2&field_id=image&fldr=banner&&akey=cfRAHqgOpv" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
      </div>
    </div>
  </div>
</div>

<?php echo form_close(); ?>