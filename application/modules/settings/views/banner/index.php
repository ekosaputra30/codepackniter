<?php echo form_open($form_action); ?>
<div class="card">
  <div class="card-header">
    <div class="btn-group bulk-menu d-none" role="group" aria-label="">
      <input type="hidden" name="bulk-method" value="delete">
      <button type="submit" onclick="return confirm('This action will delete selected items, are you sure?')" class="btn btn-danger"><i class="cil-trash"></i> delete selected items</button>
    </div>

    <a href="<?php echo module_url('create') ?>" class="btn btn-primary float-right">add page</a>
  </div>
  <div class="card-body">
    <div class="d-none table-responsive">
      <table width="100%" class="table table-condensed">
        <thead>
          <tr class="d-flex">
            <th class="col-sm-1 border-top-0">
              <div class="form-group">
                <div class="form-check checkbox">
                  <input type="checkbox" name="all_page" id="check_all" class="bulk-checkbox-all form-check-input" onclick="App.toggleCheckAll()">
                </div>
              </div>
            </th>
            <th class="col-sm-5 border-top-0">Page</th>
            <th class="col-sm-3 border-top-0">Module</th>
            <th class="col-sm-3 border-top-0"># &nbsp;</th>
          </tr>
        </thead>
        <tbody>
          <?php if($banners->count == 0): ?>
            <tr>
              <td colspan="3">no record available</td>
            </tr>
          <?php else:?>
            <?php foreach($banners->result as $k => $banner):?>
              <tr class="d-flex">
                <td class="col-sm-1">
                  <div class="form-group">
                    <div class="form-check checkbox">
                      <input type="checkbox" name="id[]" id="check-<?php echo $k+1 ?>" class="bulk-checkbox form-check-input" onclick="App.toggleCheck(<?php echo $k+1 ?>)" value="<?php echo $banner['id'] ?>">
                    </div>
                  </div>
                </td>
                <td class="col-sm-5"><?php echo $banner['page_name'] ?></td>
                <td class="col-sm-3"><?php echo $banner['module_name'] ?></td>
                <td class="col-sm-3">
                  <div class="btn-group" role="group" aria-label="">
                    <a href="<?php echo module_url('edit/'.$banner['id']) ?>" class="btn btn-info"><i class="cil-pencil"></i> edit</a>
                    <a href="<?php echo module_url('destroy/'.$banner['id']) ?>" class="btn btn-danger" onclick="return confirm('are you sure?');"><i class="cil-trash"></i> delete</a>
                  </div>
                </td>
              </tr>
            <?php endforeach;?>
          <?php endif;?>
        </tbody>
      </table>
    </div>
    <div class="row">
      <div class="col-sm-4 col-xs-6">
        <div class="card">
          <img class="card-img" src="https://placehold.it/314x167/" alt="Card image cap">
          <div class="card-img-overlay d-flex justify-content-center align-items-center">
            <a href="<?php echo module_url('create') ?>" class="btn btn-lg btn-primary btn-square card-title"><i class="cil-plus"></i> add banner</a>
          </div>
        </div>
      </div>
      <?php foreach($banners->result as $k => $banner): ?>
        <div class="col-sm-4 col-xs-6">
          <div class="card card-banner text-white">
            <img class="card-img" src="<?php echo $banner['image'] ?>" alt="Card image cap">
            <div class="card-img-overlay d-flex flex-column justify-content-center align-items-center">
              <h4 class="card-text"><?php echo $banner['name'] ?></h4>
              <div class="btn-group" role="group" aria-label="">
                <a href="<?php echo module_url('edit/'.$banner['id']) ?>" class="btn btn-square btn-info"><i class="cil-pencil"></i></a>
                <a href="<?php echo module_url('show/'.$banner['id']) ?>" class="btn btn-square btn-light"><i class="cil-magnifying-glass"></i></a>
                <a href="<?php echo module_url('destroy/'.$banner['id']) ?>" class="btn btn-square btn-danger" onclick="return confirm('Are you sure?')"><i class="cil-trash"></i></a>
              </div>
            </div>
          </div>
        </div>
      <?php endforeach;?>
    </div>
  </div>
</div>
<?php echo form_close(); ?>