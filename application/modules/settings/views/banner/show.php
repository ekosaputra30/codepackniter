<div class="row">
  <div class="col">
    <a href="<?php echo module_url() ?>" class="btn btn-default btn-square"><i class="cil-arrow-left"></i></a>
  </div>
</div>
<div class="d-flex flex-column-reverse flex-sm-row">
  <div class="col-12 col-sm-6">
    <div class="card">
      <div class="card-header">
        <h4>Meta</h4>
      </div>
      <div class="card-body">
        <div class="form-group row">
          <label for="name" class="col-sm-4 col-form-label">Block Name</label>
          <div class="col-sm-8">
            <input type="text" value="<?php echo $result['name'] ?>" name="name" id="name" class="form-control-plaintext" placeholder="" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="description" class="col-sm-4 col-form-label">Short Description</label>
          <div class="col-sm-8">
            <textarea name="description" id="description" class="form-control-plaintext" placeholder=""><?php echo $result['description'] ?></textarea>
          </div>
        </div>
        <div class="form-group row">
          <label for="heading" class="col-sm-4 col-form-label">Heading Text</label>
          <div class="col-sm-8">
            <input type="text" value="<?php echo $result['heading'] ?>" name="heading" id="heading" class="form-control-plaintext" placeholder="">
          </div>
        </div>
        <div class="form-group row">
          <label for="sub" class="col-sm-4 col-form-label">Sub Heading Text</label>
          <div class="col-sm-8">
            <input type="text" value="<?php echo $result['sub'] ?>" name="sub" id="sub" class="form-control-plaintext" placeholder="">
          </div>
        </div>
        <div class="form-group row">
          <label for="content" class="col-sm-4 col-form-label">Content</label>
          <div class="col-sm-8">
            <textarea name="content" id="content" class="form-control-plaintext" placeholder=""><?php echo $result['content'] ?></textarea>
          </div>
        </div>
        <div class="form-group row">
          <label for="style" class="col-sm-4 col-form-label">Custom Style</label>
          <div class="col-sm-8">
            <textarea name="style" id="style" class="form-control-plaintext" placeholder=""><?php echo $result['style'] ?></textarea>
          </div>
        </div>
        <div class="form-group row">
          <label for="page" class="col-form-label col-sm-4">Page</label>
          <div class="col-sm-4">
            <select name="page" id="page" class="form-control-plaintext">
              <option value="0">-choose page-</option>
              <?php foreach($pages->result as $k => $page):?>
                <option value="<?php echo $page['id'] ?>" <?php echo ($result['page'] == $page['id']) ? 'selected="selected"' : false ?>><?php echo $page['page_name'] ?></option>
              <?php endforeach;?>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-sm-6">
    <div class="card">
      <div class="card-header">
        <h4>Image</h4>
      </div>
      <div class="card-body">
        <div class="form-group row <?php echo ($result['image'] != '' ? false : 'd-none') ?>" id="preview_image_block">
          <label for="image" class="col-sm-4 col-form-label">Preview</label>
            <div class="col-sm-8">
              <img src="<?php echo $result['image'] ?>" id="preview_image" alt="" class="img-thumbnail">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>