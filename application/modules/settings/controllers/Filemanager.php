<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Filemanager extends Admin_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->_module    = 'settings';
    $this->_page      = 'file';
  }

  public function index()
  {
    $this->_current   = 'file manager';

    $data = [
      'page_view' => $this->_module.'/'.$this->_page.'/index',
      'page_data' => []
    ];
    
    return $this->_render($data);
  }
}

/* End of file Filemanager.php */
