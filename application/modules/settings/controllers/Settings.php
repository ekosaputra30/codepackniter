<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Admin_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->_module    = 'settings';
    $this->_page    = 'settings';

    $this->load->model('settings/Setting_model');
    
  }

  public function index()
  {
    $this->_current   = 'index';
    $settings = $this->Setting_model->get_all();

    $data = [
      'page_view' => $this->_module.'/'.$this->_page.'/index',
      'page_data' => [
        'settings' => $settings,
        'form_action' => module_url('save'),
      ]
    ];
    
    return $this->_render($data);
  }

  public function save() {
    $method = $this->input->server('REQUEST_METHOD');
    if($method === 'POST') {
      $post = $this->input->post();

      $this->Setting_model->deleteAll();
      foreach ($post['setting-name'] as $key => $name) {
        $content = ($post['setting-text'][$key] !== "") ? $post['setting-text'][$key] : $post['setting-image'][$key];
        $insert = [
          'type' => $post['setting-type'][$key],
          'name' => $name,
          'content' => $content,
          'slug' => url_title($name, '-', TRUE),
          'status' => 1
        ];

        //if(isset($post['setting-id'][$key])) $this->Setting_model->edit($post['setting-id'][$key], $insert);
        //else $this->Setting_model->save($insert);
        $this->Setting_model->save($insert);
      }

      $this->set_fdata('cb:form:success', 'Data has been saved');
      redirect(site_url($this->_module.'/'.$this->_page)); 
    } else {
      echo 'Oops!';
    }
  }

  public function loadfilemanager($id = false) {
    echo $this->load->view($this->_module.'/'.$this->_page.'/load', ['id'=> $id]);
  }

}

/* End of file Settings.php */
