<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Banner extends Admin_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->_module    = 'settings';
    $this->_page      = 'banner';
    $this->load->model('settings/Page_model');
    $this->load->model('settings/Block_model');
    $this->load->model('settings/Banner_model');
  }
  
  public function index() {
    $this->_current   = 'index';
    $banners = $this->Banner_model->get_all();
    
    $data = [
      'page_view' => $this->_module.'/'.$this->_page.'/index',
      'page_data' => [
        'banners'       => $banners,
        'form_action' => module_url('bulk')
      ]
    ];

    $this->_render($data);
  }
  
  public function create() {
    $this->load->library('Form_validation');
    $this->form_validation->set_rules('name', 'Block Name', 'trim|required');
    $this->form_validation->set_rules('image', 'Image', 'trim|required');

    if ($this->form_validation->run() == TRUE) {
      $post = $this->input->post();
      $insert = [
        'page' => $post['page'],
        'name' => $post['name'],
        'description' => $post['description'],
        'heading' => $post['heading'],
        'sub' => $post['sub'],
        'content' => $post['content'],
        'style' => $post['style'],
        'image' => $post['image'],
        'created_at' => date('Y-m-d H:i:s'),
        'status' => 1,
      ];

      if ($id = $this->Block_model->save($insert)) {
        $bannerinsert = [
          'block' => $id,
          'created_at' => date('Y-m-d H:i:s'),
          'status' => 1,
        ];
        if ($this->Banner_model->save($bannerinsert)) {
          $this->set_fdata('cb:form:success', 'Data has been saved');
          redirect(module_url());
        }
      }
    } else {      
      $this->_current   = 'create';
      $pages = $this->Page_model->get_all(TRUE, 'ASC');
      $data = [
        'page_view' => $this->_module.'/'.$this->_page.'/create',
        'page_data' => [
          'pages' => $pages,
          'form_action' => module_url('create')
        ]
      ];
    }

    $this->_render($data);
  }
  
  public function edit($id = 0) {
    $this->load->library('Form_validation');
    $this->form_validation->set_rules('name', 'Block Name', 'trim|required');
    $this->form_validation->set_rules('image', 'Image', 'trim|required');

    if ($this->form_validation->run() == TRUE) {
      $post = $this->input->post();
      $banner = $this->Banner_model->get($id)->result;
      
      $insert = [
        'page' => $post['page'],
        'name' => $post['name'],
        'description' => $post['description'],
        'heading' => $post['heading'],
        'sub' => $post['sub'],
        'content' => $post['content'],
        'style' => $post['style'],
        'image' => $post['image']
      ];

      if ($this->Block_model->edit($banner['block_id'], $insert)) {
        $this->set_fdata('cb:form:success', 'Data has been saved');
        redirect(module_url());
      }
    } else {      
      $this->_current   = 'edit';
      $pages = $this->Page_model->get_all(TRUE, 'ASC');
      $result = $this->Banner_model->get($id)->result;
      
      $data = [
        'page_view' => $this->_module.'/'.$this->_page.'/edit',
        'page_data' => [
          'result' => $result,
          'pages' => $pages,
          'form_action' => module_url('edit/'.$id)
        ]
      ];
    }

    $this->_render($data);
  }

  public function show($id = 0) {
    $this->_current   = 'show';
    $pages = $this->Page_model->get_all(TRUE, 'ASC');
    $result = $this->Banner_model->get($id)->result;
    
    $data = [
      'page_view' => $this->_module.'/'.$this->_page.'/show',
      'page_data' => [
        'result' => $result,
        'pages' => $pages,
      ]
    ];

    $this->_render($data);
  }

  public function destroy($id = 0) {
    if ($this->Banner_model->delete($id)) {
      $this->set_fdata('cb:form:success', 'Data has been deleted');
      redirect(module_url());
    }
  }
}