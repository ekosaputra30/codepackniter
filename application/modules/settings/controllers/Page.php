<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Page extends Admin_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->_module    = 'settings';
    $this->_page      = 'page';
    $this->load->model('settings/Page_model');
  }
  
  public function index() {
    $this->_current   = 'index';
    $data = [
      'page_view' => $this->_module.'/'.$this->_page.'/index',
      'page_data' => [
        'pages'       => $this->Page_model->get_all(TRUE, 'ASC'),
        'form_action' => module_url('bulk')
      ]
    ];

    $this->_render($data);
  }
  
  public function create() {
    $this->_current   = 'create';
    $data = [
      'page_view' => $this->_module.'/'.$this->_page.'/create',
      'page_data' => [
        'form_action' => module_url('store')
      ]
    ];

    $this->_render($data);
  }
  
  public function edit($id = 0) {
    $this->_current   = 'edit';

    $result = $this->Page_model->get($id);
    $data = [
      'page_view' => $this->_module.'/'.$this->_page.'/edit',
      'page_data' => [
        'id' => $id,
        'result' => $result->result,
        'form_action' => base_url('settings/page/update')
      ]
    ];

    $this->_render($data);
  }

  public function store() {
    $this->load->library('Form_validation');

    $this->form_validation->set_rules('page_name', 'Page', 'trim|required');
    $this->form_validation->set_rules('module_name', 'Module', 'trim|required');
    
    if ($this->form_validation->run() == TRUE) {
      $post = $this->input->post();

      $insert = [
        'page_name'       => $post['page_name'],
        'module_name'     => $post['module_name'],
        'sort_num'        => $post['sort_num'],
        'slug'            => $post['slug'],
        'created_at'      => date('Y-m-d H:i:s'),
        'status'          => 1
      ];

      if ($this->Page_model->save($insert)) {
        $this->set_fdata('cb:form:success', 'Data has been saved');
        redirect(module_url());
      }
    } else {
      $this->set_fdata('cb:form:alert', validation_errors());
      redirect(module_url('create'));
    }
  }

  public function update() {
    $this->load->library('Form_validation');

    $this->form_validation->set_rules('page_name', 'Page', 'trim|required');
    $this->form_validation->set_rules('module_name', 'Module', 'trim|required');
    
    if ($this->form_validation->run() == TRUE) {
      $post = $this->input->post();

      $update = [
        'page_name'      => $post['page_name'],
        'module_name'    => $post['module_name'],
        'sort_num'       => $post['sort_num'],
        'slug'           => $post['slug'],
      ];

      if ($this->Page_model->edit($post['id'], $update)) {
        $this->set_fdata('cb:form:success', 'Data has been updated');
        redirect(module_url());
      }
    } else {
      $this->set_fdata('cb:form:alert', validation_errors());
      redirect(module_url('edit/'.$post['id']));
    }
  }

  public function destroy() {

  }

}

/* End of file Home.php */
