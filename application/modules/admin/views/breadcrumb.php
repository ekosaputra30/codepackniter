
          <!-- Breadcrumb-->
          <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item"><a href="<?php echo admin_url() ?>">Dashboard</a></li>
            <?php if ($this->_page !== 'admin'): ?>
              <li class="breadcrumb-item"><a href="<?php echo base_url($this->_module.'/'.$this->_page) ?>"><?php echo $this->_page ?></a></li>
            <?php endif; ?>
            <li class="breadcrumb-item active"><?php echo $this->_current ?></li>
          </ol>