<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Page extends Admin_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->_module    = 'settings';
    $this->_page      = 'page';
  }
  
  public function index() {
    $this->_current   = 'index';
    $data = [
      'page_view' => $this->_module.'/'.$this->_page.'/index',
      'page_data' => []
    ];

    $this->_render($data);
  }
  
  public function create() {
    $this->_current   = 'create';
    $data = [
      'page_view' => $this->_module.'/'.$this->_page.'/create',
      'page_data' => []
    ];

    $this->_render($data);
  }
  
  public function edit() {
    $this->_current   = 'edit';
    $data = [
      'page_view' => $this->_module.'/'.$this->_page.'/edit',
      'page_data' => []
    ];

    $this->_render($data);
  }

  public function store() {

  }

  public function update() {
    
  }

  public function destroy() {
    
  }

}

/* End of file Home.php */
