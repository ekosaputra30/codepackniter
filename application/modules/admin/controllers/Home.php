<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends Admin_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->_module    = 'admin';
    $this->_page      = 'home';
  }
  
  public function index() {
    $this->_current   = 'index';
    $data = [
      'page_view' => $this->_module.'/'.$this->_page.'/index',
      'page_data' => []
    ];

    $this->_render($data);
  }

}

/* End of file Home.php */
