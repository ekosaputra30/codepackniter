<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller {
  private $password;
  public function __construct()
  {
    parent::__construct();
    $this->_module    = 'admin';
    $this->_page      = 'admin';
    $this->password   = 'cms@admin_!@#';
  }
  
  public function index() {
    $this->_current   = 'index';

    $user = $this->auth->getByEmail($this->dash->getUser('email'));
    $data = [
      'page_view' => $this->_module.'/'.$this->_page.'/index',
      'page_data' => [
        'user' => $user
      ]
    ];

    $this->_render($data);
  }

  public function new_password() {
    echo $this->password;
    echo "<br>";
    echo password_hash($this->password, PASSWORD_DEFAULT, ['cost' => 10]);
  }

}

/* End of file Landing.php */
