<?php

class User_model extends CI_Model
{
  private $_table = 'users';

  public function get($email = false) {
    $return = new ArrayObject();
    $query = $this->db->get_where($this->_table, ['email' => $email]);

    $return->count  = $query->num_rows();
    $return->result = $query->row_array();

    return $return;  
  }

  public function get_all() {
    $return = new ArrayObject();
    $this->db->order_by('id', 'DESC');
    $query = $this->db->get($this->_table);
    
    $return->count  = $query->num_rows();
    $return->result = $query->result_array();

    return $return;
  }
}
