<?php echo form_open($form_action); ?>
<div class="card">
  <div class="card-header">
    <div class="btn-group bulk-menu d-none" role="group" aria-label="">
      <input type="hidden" name="bulk-method" value="delete">
      <button type="submit" onclick="return confirm('This action will delete selected items, are you sure?')" class="btn btn-square btn-danger"><i class="cil-trash"></i> delete selected items</button>
    </div>
    <div class="float-right"><a href="<?php echo module_url('create') ?>" class="btn btn-primary">Add user</a></div>
  </div>
  <div class="card-body">
    <table class="table table-hovered">
      <thead>
        <tr>
          <th class="border-top-0">
            <div class="form-group">
              <div class="form-check checkbox">
                <input type="checkbox" name="all_page" id="check_all" class="bulk-checkbox-all form-check-input" onclick="App.toggleCheckAll()">
              </div>
            </div>
          </th>
          <th class="border-top-0">User</th>
          <th class="border-top-0">Role</th>
          <th class="border-top-0"></th>
        </tr>
      </thead>
      <tbody>
        <?php if(count($users) == 0): ?>
          <tr>
            <td colspan="4">no record available</td>
          </tr>
        <?php else:?>
          <?php foreach($users as $k => $user):?>
            <tr>
              <td scope="row">
                <div class="form-group">
                  <div class="form-check checkbox">
                    <input type="checkbox" name="id[]" id="check-<?php echo $k+1 ?>" class="bulk-checkbox form-check-input" onclick="App.toggleCheck(<?php echo $k+1 ?>)" value="<?php echo $user['fields']['id'] ?>">
                  </div>
                </div>
              </td>
              <td><?php echo $user['fields']['email'] ?></td>
              <td><?php echo key($user['roles']) ?></td>
              <td>
                <div class="btn-group">
                  <a href="<?php echo module_url('edit/'.$user['fields']['id']) ?>" class="btn btn-square btn-info">edit</a>
                  <a href="<?php echo module_url('show/'.$user['fields']['id']) ?>" class="btn btn-square btn-light">view</a>
                  <a href="<?php echo module_url('destroy/'.$user['fields']['id']) ?>" class="btn btn-square btn-danger" onclick="return confirm('are you sure?')">delete</a>
                </div>
              </td>
            </tr>
          <?php endforeach;?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>
<?php echo form_close(); ?>