  <div class="row">
    <div class="col-6">
      <?php if(validation_errors()): ?>
        <div class="alert alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <?php echo validation_errors(); ?>
        </div>
      <?php endif;?>
      <div class="card">
        <div class="card-header">
          <div class="float-left">
            <a href="<?php echo module_url() ?>" id="c-icon-details" class="btn btn-default">
              <i class="cil-arrow-left"></i>
            </a>
          </div>
          <div class="float-right">
            <button type="submit" class="btn btn-success">submit</button>
          </div>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label for="display_name" class="col-sm-4 col-form-label">Full name</label>
            <div class="col-sm-8">
              <input type="text" name="display_name" id="display_name" class="form-control-plaintext" value="<?php echo ': ' . $result['fields']['display_name'];?>" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="email" class="col-sm-4 col-form-label">Email</label>
            <div class="col-sm-8">
              <input type="email" name="email" id="email" class="form-control-plaintext" value="<?php echo ': ' . $result['fields']['email'];?>" readonly>
            </div>
          </div>
          <div class="form-group row">
            <label for="role" class="col-sm-4 col-form-label">Role</label>
            <div class="col-sm-8">
              <input type="text" class="form-control-plaintext toggle-element-target" id="user_role_target_new" name="role_new" placeholder="add new role" value="<?php echo ': ' . key($result['roles']);?>" readonly>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-8 offset-sm-4">
              <a href="<?php echo module_url('edit/'.$id); ?>" class="btn btn-square btn-info">Edit</a>
              <a href="<?php echo module_url(); ?>" class="btn btn-square btn-default">Cancel</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
