<?php echo form_open($form_action); ?>
  <div class="card">
    <div class="card-header">
      <div class="float-right"><a href="<?php echo module_url('create') ?>" class="btn btn-primary">Add user</a></div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-6">
          <div class="form-group row">
            <label for="role" class="col-sm-4 col-form-label">Role Name</label>
            <div class="col-sm-8">
              <input type="text" name="role" id="role" class="form-control" placeholder="ex: 'admin' or 'developer" aria-describedby="help-role">
            </div>
          </div>
          <div class="form-group row">
            <label for="perm" class="col-sm-4 col-form-label">Permissions</label>
            <div class="col-sm-8">
              <?php foreach($permissions as $k => $perm):?>
                <div class="form-check">
                  <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="perm[]" id="perm-<?php echo $k+1 ?>" value="<?php echo $perm['perm_id'] ?>"> <?php echo $perm['perm_desc'] ?>
                  </label>
                </div>
              <?php endforeach;?>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-8 offset-sm-4">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php echo form_close(); ?>
