<div class="card">
  <div class="card-header">
    <div class="float-right">
      <a href="<?php echo site_url('rbac/permission') ?>" class="btn btn-primary">Permissions</a>
      <a href="<?php echo module_url('create') ?>" class="btn btn-primary">Add Role</a>
    </div>
  </div>
  <div class="card-body">
    <table class="table table-hovered">
      <thead>
        <tr>
          <th class="border-top-0">No</th>
          <th class="border-top-0">Role</th>
          <th class="border-top-0">Permissions</th>
          <th class="border-top-0"></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($roles as $k => $role):?>
          <tr>
            <td scope="row"><?php echo $k+1 ?></td>
            <td><?php echo ($role['role_name']) ?></td>
            <td>
              <?php if(count($role['roles']) > 0):?>
                <?php foreach($role['roles'] as $j => $role2):?>
                  <button type="button" class="badge badge-pill badge-secondary">
                    <?php echo $j ?>
                  </button>
                <?php endforeach;?>
              <?php else:?>
                <a href="<?php echo site_url('rbac/permission/create') ?>" class="btn btn-sm btn-primary"><i class="cil-plus"></i> add permission</a>
              <?php endif ?>
            </td>
            <td>
              <div class="btn-group">
                <a href="<?php echo module_url('edit/'.$role['role_id']) ?>" class="btn btn-square btn-info">edit</a>
                <a href="<?php echo module_url('show/'.$role['role_id']) ?>" class="btn btn-square btn-light">view</a>
                <a href="<?php echo module_url('destroy/'.$role['role_id']) ?>" class="btn btn-square btn-danger" onclick="return confirm('are you sure?')">delete</a>
              </div>
            </td>
          </tr>
        <?php endforeach;?>
      </tbody>
    </table>
  </div>
</div>