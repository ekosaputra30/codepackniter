<?php echo form_open($form_action); ?>
  <div class="row">
    <div class="col-6">
      <?php if(validation_errors()): ?>
        <div class="alert alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <?php echo validation_errors(); ?>
        </div>
      <?php endif;?>
      <div class="card">
        <div class="card-header">
          <div class="float-left">
            <a href="<?php echo module_url() ?>" id="c-icon-details" class="btn btn-default">
              <i class="cil-arrow-left"></i>
            </a>
          </div>
        </div>
        <div class="card-body">
          <div class="form-group row">
            <label for="perm_desc" class="col-sm-4 col-form-label">Permission</label>
            <div class="col-sm-8">
              <input type="text" name="perm_desc" id="perm_desc" class="form-control" value="<?php echo set_value('perm_desc');?>" required>
            </div>
          </div>
          <div class="form-group row mb-0">
            <label for="role" class="col-sm-4 col-form-label">Role</label>
            <div class="col-sm-8">
              <div class="form-check form-check-inline">
                <input class="form-check-input toggle-element" onchange="App.toggleElement('#user_role_target_exist')" type="radio" name="user_role" data-target="#user_role_target_exist" id="user_role_exist" value="exist" checked>
                <label class="form-check-label" for="user_role_exist">exist role</label>
              </div>
              <div class="form-check form-check-inline">
                <input class="form-check-input toggle-element" onchange="App.toggleElement('#user_role_target_new')" type="radio" name="user_role" data-target="#user_role_target_new" id="user_role_new" value="new">
                <label class="form-check-label" for="user_role_new">new role</label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-8 offset-sm-4">
              <div class="toggle-element-target" id="user_role_target_exist">
                <?php foreach($roles as $r => $role): ?>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" name="role_exist[]" id="role-<?php echo $r+1 ?>" value="<?php echo $role['role_id'] ?>">
                    <label class="form-check-label" for="role-<?php echo $r+1 ?>"><?php echo $role['role_name'] ?></label>
                  </div>
                <?php endforeach;?>
              </div>
              <input type="text" class="form-control d-none toggle-element-target" id="user_role_target_new" name="role_new" placeholder="add new role">
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-8 offset-sm-4">
              <button type="submit" class="btn btn-success">Submit</button>
              <a href="<?php echo module_url(); ?>" class="btn btn-default">Cancel</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php echo form_close(); ?>