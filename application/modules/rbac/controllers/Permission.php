<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
    $this->_module    = 'rbac';
    $this->_page      = 'permission';
    $this->load->library('Auth');
    
    $this->load->model('Role_model');
    $this->load->model('Privileged_model');
    $this->load->model('Permission_model');
	}
	
	public function index()
	{
    $this->_current   = 'index';
    $permissions = $this->auth->getPermissions();

		$data = [
      'page_view' => $this->_module.'/'.$this->_page.'/index',
      'page_data' => [
        'permissions' => $permissions,
        'form_action' => module_url('bulk')
      ]
		];
		$this->_render($data);
  }

  public function create() {
    $this->load->library('Form_validation');
    $this->form_validation->set_rules('perm_desc', 'permission', 'trim|required');

    if ($this->form_validation->run() == TRUE) {
      $post = $this->input->post();
      $insert = [
        'perm_desc' => $post['perm_desc']
      ];

      if ($id = $this->Permission_model->save($insert)) {
        if($post['user_role'] == 'exist') {
          $id_role = $post['role_exist'];
          foreach ($id_role as $k => $idr) {
            $this->Privileged_model->insertPerm($idr, $id);
          }
        }
        else {
          $id_role = $this->Role_model->insertRole($post['role_new']);
          $this->Privileged_model->insertPerm($id_role, $id);
        }

        $this->set_fdata('cb:form:success', 'Data has been saved');
        redirect(module_url());
      }
    } else {
      $this->_current   = 'create';
      $roles = $this->Role_model->getRoles();
      
      $data = [
        'page_view' => $this->_module.'/'.$this->_page.'/create',
        'page_data' => [
          'roles' => $roles,
          'form_action' => module_url('create')
        ]
      ];
      $this->_render($data);
    }
  }

  public function edit($id) {
    $this->load->library('Form_validation');
    $this->form_validation->set_rules('perm_desc', 'permission', 'trim|required');

    if ($this->form_validation->run() == TRUE) {
      $post = $this->input->post();
      $insert = [
        'perm_desc' => $post['perm_desc']
      ];

      if ($this->Permission_model->edit($id, $insert)) {
        if($post['user_role'] == 'exist') {
          $id_role = $post['role_exist'];
          foreach ($id_role as $k => $idr) {
            $this->Privileged_model->deleteRoles($idr, $id);
            $this->Privileged_model->insertPerm($idr, $id);
          }
        }
        else {
          $id_role = $this->Role_model->insertRole($post['role_new']);
          $this->Privileged_model->insertPerm($id_role, $id);
        }

        $this->set_fdata('cb:form:success', 'Data has been saved');
        redirect(module_url());
      }
    } else {
      $this->_current   = 'edit';
      $perm = $this->Permission_model->get($id)->result;
      
      $rs = $this->Role_model->getRoles();
      $roles_active = $this->Privileged_model->getRoles($perm['perm_id']);

      $roles = [];
      foreach ($rs as $key => $role) {
        $roles[$key] = $role;
        $roles[$key]['is_checked'] = false;
        foreach ($roles_active as $j => $ac) {
          if($role['role_id'] === $ac['role_id']) $roles[$key]['is_checked'] = true;
        }
      }

      $data = [
        'page_view' => $this->_module.'/'.$this->_page.'/edit',
        'page_data' => [
          'id' => $id,
          'roles' => $roles,
          'result' => $perm,
          'form_action' => module_url('edit/'.$id)
        ]
      ];
      $this->_render($data);
    }
  }

  public function bulk() {
    $post = $this->input->post('id');
    foreach ($post as $key => $id) {
      $this->Privileged_model->deleteSelectedPerms($id);
    }

    $this->set_fdata('cb:form:success', 'Data has been deleted');
    redirect(module_url());
  }
}