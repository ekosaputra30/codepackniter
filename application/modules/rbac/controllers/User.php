<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
    $this->_module    = 'rbac';
    $this->_page      = 'user';
    $this->load->library('Auth');
    $this->load->model('rbac/User_model');
	}
	
	public function index()
	{
    $this->_current   = 'index';
		$users = $this->auth->getUsers();

		$data = [
      'page_view' => $this->_module.'/'.$this->_page.'/index',
      'page_data' => [
        'users' => $users,
        'form_action' => module_url('bulk')
      ]
		];
		$this->_render($data);
	}

	public function create() {
    $this->_current   = 'create';
		$this->load->library('Form_validation');
	
		$this->form_validation->set_rules('display_name', 'Full name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');
		
		if ($this->form_validation->run() === TRUE) {
      $post     = $this->input->post();
      $id_role  = 0;
      $password = password_hash($post['password'], PASSWORD_DEFAULT, ['const' => 12]);

      $insert = [
        'display_name' => $post['display_name'],
        'email' => $post['email'],
        'password' => $password,
        'created_at' => date('Y-m-d H:i:s'),
        'status' => 1
      ];
      
      if ($id_user = $this->User_model->save($insert)) {
        $this->load->model('Role_model');
        
        if($post['user_role'] == 'exist') $id_role = $post['role_exist'];
        else {
          $id_role = $this->Role_model->insertRole($post['role_new']);
        }

        $this->Role_model->insertUserRoles($id_user, $id_role);

        $this->set_fdata('cb:form:success', 'Data has been saved');
        redirect(module_url());
      }
		} else {
			$data = [
        'page_view' => $this->_module.'/'.$this->_page.'/create',
        'page_data' => [
          'form_action' => module_url('create'),
          'roles' => $this->auth->getRoles(),
          'permissions' => $this->auth->getPermissions()
        ]
			];
			
			$this->_render($data);
		}
  }

  public function show($id_user) {
    $this->_current   = 'view';
    $user = $this->User_model->get($id_user)->result;
    $row = $this->auth->getByEmail($user['email']);
    $data = [
      'page_view' => $this->_module.'/'.$this->_page.'/view',
      'page_data' => [
        'roles' => $this->auth->getRoles(),
        'id' => $id_user,
        'result' => $row,
        'permissions' => $this->auth->getPermissions()
      ]
    ];
    
    $this->_render($data);
  }

	public function edit($id_user = 0) {
    $this->_current   = 'edit';
		$this->load->library('Form_validation');
	
		$this->form_validation->set_rules('display_name', 'Full name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|matches[password]');
		
		if ($this->form_validation->run() === TRUE) {
      $post     = $this->input->post();
      $id_role  = 0;
      $password = password_hash($post['password'], PASSWORD_DEFAULT, ['const' => 12]);

      $insert = [
        'display_name' => $post['display_name'],
        'email' => $post['email'],
        'created_at' => date('Y-m-d H:i:s'),
        'status' => 1
      ];

      if (!empty($post['password'])) {
        $insert['password'] = $password;
      }
      
      if ($this->User_model->edit($id_user, $insert)) {
        $this->load->model('Role_model');
        
        if($post['user_role'] == 'exist') $id_role = $post['role_exist'];
        else {
          $id_role = $this->Role_model->insertRole($post['role_new']);
        }

        $this->Role_model->deleteUserRoles($id_user);
        $this->Role_model->insertUserRoles($id_user, $id_role);

        $this->set_fdata('cb:form:success', 'Data has been saved');
        redirect(module_url());
      }
		} else {
      $user = $this->User_model->get($id_user)->result;
      $row = $this->auth->getByEmail($user['email']);
			$data = [
        'page_view' => $this->_module.'/'.$this->_page.'/edit',
        'page_data' => [
          'form_action' => module_url('edit/'.$id_user),
          'roles' => $this->auth->getRoles(),
          'id' => $id_user,
          'result' => $row,
          'permissions' => $this->auth->getPermissions()
        ]
			];
			
			$this->_render($data);
		}
  }
  
  public function destroy($id) {
    if ($this->User_model->delete($id)) {
      $this->set_fdata('cb:form:success', 'Data has been deleted');
      redirect(module_url());
    }
  }

  public function bulk() {
    $this->load->model('Role_model');
    $post = $this->input->post();
    
    foreach ($post['id'] as $k => $v) {
      $this->Role_model->deleteUserRoles($v);
      $this->User_model->delete($v);
    }

    $this->set_fdata('cb:form:success', 'Data has been deleted');
    redirect(module_url());
  }
}
