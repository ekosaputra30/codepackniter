<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
    $this->_module    = 'rbac';
    $this->_page      = 'role';
		$this->load->library('Auth');
	}
	
	public function index()
	{
    $this->_current   = 'index';
		$roles = $this->auth->getRoles();

		$data = [
      'page_view' => $this->_module.'/'.$this->_page.'/index',
      'page_data' => [
        'roles' => $roles,
        'form_action' => module_url('bulk')
      ]
		];
		$this->_render($data);
	}

	public function create() {
    $this->_current   = 'create';
		$this->load->library('Form_validation');
	
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		
		if ($this->form_validation->run() === TRUE) {
			$post = $this->input->post();
			
			echo "<pre>";
			print_r ($post);
			echo "</pre>";

			echo validation_errors();
		} else {
			$data = [
        'page_view' => $this->_module.'/'.$this->_page.'/create',
        'page_data' => [
          'form_action' => module_url('save'),
          'permissions' => $this->auth->getPermissions()
        ]
			];
			
			$this->_render($data);
		}
	}
}
