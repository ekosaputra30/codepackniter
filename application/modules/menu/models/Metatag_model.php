<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Metatag_model extends CI_Model {

  protected $_table = 'metatags';

  public function getBySlug($slug = false, $type = false) {
    $return = new ArrayObject();

    $this->db->select('metatags.id AS id_meta, metatags.type, metatags.name AS meta_name, metatags.content, pages.id AS id_page, pages.page_name, pages.slug');
    $this->db->from($this->_table);
    $this->db->join('pages', 'pages.id = metatags.page');
    $this->db->where('pages.slug', $slug);
    if ($type) $this->db->where('metatags.type', $type);

    $query = $this->db->get();

    $return->count  = $query->num_rows();
    $return->result = $query->result_array();
    
    return $return;
  }

  public function checkByPage($page = false) {
    return $this->db->get_where($this->_table, ['page' => $page])->num_rows();
  }

  public function save($data) {
    if ($this->db->insert($this->_table, $data)) return $this->db->insert_id();

    else return false;
  }

  public function edit($id, $data) {
    if ($this->db->update($this->_table, $data, ['id' => $id])) return true;

    else return false;
  }

  public function delete($id) {
    if ($this->db->delete($this->_table, ['id' => $id])) return true;

    return false;
  }

  public function deleteByPage($id, $type = false) {
    if ($this->db->delete($this->_table, ['page' => $id, 'type' => $type])) return true;

    return false;
  }
}

/* End of file Metatag_model.php */
