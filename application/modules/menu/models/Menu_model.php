<?php

class Menu_model extends CI_Model
{
  private $_table_page = 'pages';
  private $_table_block = 'blocks';

  public function getBySlug($slug = false) {
    $return = new ArrayObject();
    $this->db->select('pages.id AS id_page, pages.*');

    $query = $this->db->get_where($this->_table_page, ['slug' => $slug]);
    $content = $query->row_array();

    $blocks = $this->getBlocks($content['id_page']);
    $banners = $this->getbanners($slug, true, 'DESC');
    $content['blocks'] = $blocks->result;
    $content['banners'] = $banners->result;

    $return->count  = $query->num_rows();
    $return->result = $content;

    return $return;  
  }

  public function get($id = false) {
    $return = new ArrayObject();
    $this->db->select('pages.id AS id_page, pages.*');

    $query = $this->db->get_where($this->_table_page, ['id' => $id]);
    $content = $query->row_array();

    $blocks = $this->getBlocks($content['id_page']);
    $content['blocks'] = $blocks->result;

    $return->count  = $query->num_rows();
    $return->result = $content;

    return $return;  
  }

  public function getBanners($slug = false, $sort = false, $ascdesc = false) {
    $return = new ArrayObject();
    $this->db->select('banners.id AS banner_id, banners.sort_num, blocks.id AS block_id, blocks.*, pages.page_name, pages.module_name, pages.slug');
    $this->db->from('banners');
    $this->db->join('blocks', 'blocks.id = banners.block');
    $this->db->join('pages', 'pages.id = blocks.page');
    $this->db->where('pages.slug', $slug);
    $this->db->where('blocks.type', 'BANNER');
    if ($sort) $this->db->order_by('banners.sort_num', $ascdesc);

    $query = $this->db->get();

    $return->count  = $query->num_rows();
    $return->result = $query->result_array();

    return $return;  
  }

  public function getBlocks($id_page = 0) {
    $return = new ArrayObject();

    $this->db->where('blocks.type', 'CONTENT');
    $this->db->where('page', $id_page);
    $query = $this->db->get($this->_table_block);
    
    $return->count  = $query->num_rows();
    $return->result = $query->result_array();

    return $return;
  }

  public function get_all($sort = false, $ascdesc = false) {
    $return = new ArrayObject();

    if ($sort) $this->db->order_by('sort_num', $ascdesc);
    else $this->db->order_by('id', 'DESC');
    $query = $this->db->get($this->_table_page);
    
    $return->count  = $query->num_rows();
    $return->result = $query->result_array();

    return $return;
  }

  public function save($data) {
    if ($this->db->insert($this->_table_page, $data)) return $this->db->insert_id();

    else return false;
  }

  public function saveBlock($data) {
    if ($this->db->insert($this->_table_block, $data)) return $this->db->insert_id();

    else return false;
  }

  public function edit($id, $data) {
    if ($this->db->update($this->_table_page, $data, ['id' => $id])) return true;

    else return false;
  }

  public function editBlock($id, $data) {
    if ($this->db->update($this->_table_block, $data, ['id' => $id])) return true;

    else return false;
  }

  public function deleteBlock($id_block) {
    if ($this->db->delete($this->_table_block, ['id' => $id_block])) return true;

    return false;
  }

  public function deleteBlocks($id_page) {
    if ($this->db->delete($this->_table_block, ['page' => $id_page])) return true;

    return false;
  }

  public function delete($id = 0) {
    $page = $this->get($id);
    if ($page->count > 0) {
      if ($this->deleteBlocks($page->result['id_page'])) {
        $this->db->delete($this->_table_page, ['id' => $id]);

        return true;
      }
    }

    return false;
  }
}
