<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends Admin_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->_module    = 'menu';

    $this->load->model('menu/Menu_model');
    $this->load->model('menu/Metatag_model');
    $this->load->model('settings/Block_model');
    $this->load->model('settings/Banner_model');
  }

  function _remap($method = false, $args = false) {
    $this->_page = $method;
    if (method_exists($this, $method))
    {
      return $this->$method($args);
    }
    else
    {
      return $this->index($method, $args);
    }
  }

  public function index($slug = false, $args = array())
  {
    if ($args) {
      return $this->bridging($slug, $args);
    } else {
      $this->_current   = 'index';
      $page = $this->Menu_model->getBySlug($slug);
      $meta = [
        'general' => $this->Metatag_model->getBySlug($slug, 'GENERAL'),
        'og' => $this->Metatag_model->getBySlug($slug, 'OG'),
        'twittercard' => $this->Metatag_model->getBySlug($slug, 'TWITTER'),
      ];
      
      $data = [
        'page_view' => $this->_module.'/index',
        'page_data' => [
          'page'       => $page,
          'meta'       => $meta,
          'blocks'       => $page->result['blocks'],
          'form_action_block' => module_url('bulk'),
          'form_action_new' => site_url('menu/banner/create'),
          'form_action_edit' => site_url('menu/banner/edit/'.$slug),
          'form_action_setting' => site_url('menu/'.$slug.'/setting/create')
        ]
      ];
      
      return $this->_render($data);
    }
  }

  public function bridging($slug, $args = array()) {
    //$slug. ' - ' .$args[0]. ' - '. $args[1]. ' - '. $args[2];
    //'home - banner - edit - 1'
    
    $page = $args[0];
    $action = isset($args[1]) ? $args[1] : false;
    $id = isset($args[2]) ? $args[2] : false;

    //echo $slug . ' - ' . $page . ' - ' . $action . ' - ' . $id;

    if ($page == 'banner') {
      if ($action == 'create') return $this->banner_create($slug, $args);
      if ($action == 'edit') return $this->banner_edit($id, $slug);
    } elseif ($page == 'block') {
      if ($action == 'create') return $this->block_create($slug, $args);
      if ($action == 'edit') return $this->block_edit($id, $slug);
      if ($action == 'show') return $this->block_show($id, $slug);
    } elseif ($page == 'setting') {
      if ($action == 'create') return $this->setting_create($slug);
    }
  }

  public function banner_create($slug = false, $args = array()) {
    $page = $args[0];
    $action = $args[1];

    $this->load->library('Form_validation');
    $this->form_validation->set_rules('new_image', 'Image', 'trim|required');

    if ($this->form_validation->run() == TRUE) {
      $post = $this->input->post();
      $insert = [
        'type' => 'BANNER',
        'page' => $post['id_page'],
        'name' => $post['new_name'],
        'heading' => $post['new_heading'],
        'sub' => $post['new_sub'],
        'content' => $post['new_content'],
        'style' => $post['new_style'],
        'image' => $post['new_image'],
        'is_video' => isset($post['new_is_video']) ? 1 : 0,
        'created_at' => date('Y-m-d H:i:s'),
        'status' => 1,
      ];

      if(isset($post['new_is_video'])) $insert['video'] = $post['new_video'];

      if ($id = $this->Block_model->save($insert)) {
        $date = explode(' - ', $post['new_datepicker']);

        $bannerinsert = [
          'block' => $id,
          'date_start' => $date[0],
          'date_end' => $date[1],
          'created_at' => date('Y-m-d H:i:s'),
          'status' => 1,
        ];
        if ($this->Banner_model->save($bannerinsert)) {
          $this->set_fdata('cb:form:success', 'Data has been saved');
          redirect(site_url($this->_module.'/'.$slug));
        }
      }
    } else {
      $this->_current   = 'banner create';

      $pagedata = $this->Menu_model->getBySlug($slug);

      $data = [
        'page_view' => $this->_module.'/'.$page.'/create',
        'page_data' => [
          'menu' => $slug,
          'page'       => $pagedata,
          'form_action' => site_url($this->_module.'/'.$slug.'/'.'create'),
        ]
      ];
      
      return $this->_render($data);
    }
  }

  public function banner_edit($id = 0, $page = false) {
    $this->load->library('Form_validation');
    $this->form_validation->set_rules('edit_image', 'Image', 'trim|required');

    if ($this->form_validation->run() == TRUE) {
      $post = $this->input->post();
      $banner = $this->Banner_model->get($id)->result;

      $insert = [
        'page' => $post['id_page'],
        'name' => $post['edit_name'],
        'heading' => $post['edit_heading'],
        'sub' => $post['edit_sub'],
        'content' => $post['edit_content'],
        'style' => $post['edit_style'],
        'image' => $post['edit_image'],
        'is_video' => isset($post['edit_is_video']) ? 1 : 0,
      ];

      if(isset($post['edit_is_video'])) $insert['video'] = $post['edit_video'];

      if ($this->Block_model->edit($banner['block_id'], $insert)) {
        $date = explode(' - ', $post['new_datepicker']);

        $bannerinsert = [
          'date_start' => $date[0],
          'date_end' => $date[1],
        ];
        if ($this->Banner_model->edit($id, $bannerinsert)) {
          $this->set_fdata('cb:form:success', 'Data has been saved');
          redirect(site_url($this->_module.'/'.$post['page_name']));
        }
      }

    } else {
      $this->_current   = 'banner edit';
      $result = $this->Banner_model->get($id)->result;      
      
      $data = [
        'page_view' => $this->_module.'/banner/edit',
        'page_data' => [
          'result' => $result,
          'page' => $page,
          'form_action' => site_url($this->_module.'/'.$this->_page.'/banner/edit/'.$id)
        ]
      ];
      return $this->_render($data);
    }
  }

  public function block_create($slug = false, $args = array()) {
    $page = $args[0];

    $this->load->library('Form_validation');
    $this->form_validation->set_rules('name', 'Image', 'trim|required');

    if ($this->form_validation->run() == TRUE) {
      $post = $this->input->post();
      $insert = [
        'type' => 'CONTENT',
        'page' => $post['id_page'],
        'name' => $post['name'],
        'description' => $post['description'],
        'heading' => $post['heading'],
        'sub' => $post['sub'],
        'content' => $post['content'],
        'style' => $post['style'],
        'link' => $post['link'],
        'image' => $post['image'],
        'created_at' => date('Y-m-d H:i:s'),
        'status' => 1,
      ];

      if ($id = $this->Block_model->save($insert)) {
        $bannerinsert = [
          'block' => $id,
          'created_at' => date('Y-m-d H:i:s'),
          'status' => 1,
        ];
        $this->set_fdata('cb:form:success', 'Data has been saved');
        redirect(site_url($this->_module.'/'.$slug.'?tab=content'));
      }
    } else {
      $this->_current   = 'content create';

      $pagedata = $this->Menu_model->getBySlug($slug);

      $data = [
        'page_view' => $this->_module.'/'.$page.'/create',
        'page_data' => [
          'menu' => $slug,
          'page' => $pagedata->result,
          'form_action' => site_url($this->_module.'/'.$slug.'/'.$page.'/create'),
        ]
      ];
      
      return $this->_render($data);
    }
  }

  public function block_edit($id = false, $page = false) {
    $this->load->library('Form_validation');
    $this->form_validation->set_rules('name', 'Image', 'trim|required');

    if ($this->form_validation->run() == TRUE) {
      $post = $this->input->post();
      $insert = [
        'name' => $post['name'],
        'description' => $post['description'],
        'heading' => $post['heading'],
        'sub' => $post['sub'],
        'content' => $post['content'],
        'style' => $post['style'],
        'link' => $post['link'],
        'image' => $post['image'],
      ];

      if ($this->Block_model->edit($id, $insert)) {
        $this->set_fdata('cb:form:success', 'Data has been saved');
        redirect(site_url($this->_module.'/'.$this->_page.'?tab=content'));
      }
    } else {
      $this->_current   = 'content edit';
      $block = $this->Block_model->get($id)->result;

      $data = [
        'page_view' => $this->_module.'/block/edit',
        'page_data' => [
          'menu' => $page,
          'block' => $block,
          'form_action' => site_url($this->_module.'/'.$this->_page.'/block/edit/'.$id),
        ]
      ];
      
      return $this->_render($data);
    }
  }

  public function block_show($id = 0, $page = false) {

    $this->_current   = 'show content';
    $block = $this->Block_model->get($id)->result;

    $data = [
      'page_view' => $this->_module.'/block/show',
      'page_data' => [
        'menu' => $page,
        'block' => $block,
      ]
    ];
    
    return $this->_render($data);
  }

  public function setting_create($slug) {
    $request = $this->input->server('REQUEST_METHOD');
    if ($request === 'POST') {
      $post = $this->input->post();

      $this->Metatag_model->deleteByPage($post['id_page'], $post['type']);
      foreach ($post['meta_name'] as $key => $name) {
        if ($post['meta_content'][$key] !== "") {
          $insert = [
            'page' => $post['id_page'],
            'type' => $post['type'],
            'name' => $name, 
            'content' => $post['meta_content'][$key],
            'created_at' => date('Y-m-d H:i:s'),
            'status' => 1
          ];

          $this->Metatag_model->save($insert);
        }
      }
      
      $this->set_fdata('cb:form:success', 'Data has been saved');
      redirect(site_url($this->_module.'/'.$this->_page.'?tab=pagesetting'));
    } else {
      echo 'hmm';
    }
  }
}

/* End of file Menu.php */
