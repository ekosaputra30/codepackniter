<?php echo form_open($form_action_setting) ?>
  <input type="hidden" name="type" value="OG">
  <input type="hidden" name="id_page" value="<?php echo $id_page ?>">
  <div class="card">
    <div class="card-header">
      <h4 class="mb-0">Open Graph</h4>
    </div>
    <div class="card-body">
      <div class="meta-list-wrapper meta-list-og">
        <?php if($meta['og']->count == 0):?>
          <div class="form-group row meta-item">
            <div class="col-sm-4"><input type="text" name="meta_name[]" class="form-control" placeholder="meta name" value="title"></div>
            <div class="col-sm-6">
              <input type="text" name="meta_content[]" class="form-control" placeholder="meta content/value" value="">
            </div>
            <div class="col-sm-2">
              <button class="btn btn-danger btn-square meta-item-button" type="button" onclick="App.clickCloned(this.id)"><i class="cil-trash"></i></button>
            </div>
          </div>
          <div class="form-group row meta-item">
            <div class="col-sm-4"><input type="text" name="meta_name[]" class="form-control" placeholder="meta name" value="type"></div>
            <div class="col-sm-6">
              <input type="text" name="meta_content[]" class="form-control" placeholder="meta content/value" value="">
            </div>
            <div class="col-sm-2">
              <button class="btn btn-danger btn-square meta-item-button" type="button" onclick="App.clickCloned(this.id)"><i class="cil-trash"></i></button>
            </div>
          </div>
          <div class="form-group row meta-item">
            <div class="col-sm-4"><input type="text" name="meta_name[]" class="form-control" placeholder="meta name" value="image"></div>
            <div class="col-sm-6">
              <input type="text" name="meta_content[]" class="form-control" placeholder="meta content/value" value="">
            </div>
            <div class="col-sm-2">
              <button class="btn btn-danger btn-square meta-item-button" type="button" onclick="App.clickCloned(this.id)"><i class="cil-trash"></i></button>
            </div>
          </div>
          <div class="form-group row meta-item">
            <div class="col-sm-4"><input type="text" name="meta_name[]" class="form-control" placeholder="meta name" value="url"></div>
            <div class="col-sm-6">
              <input type="text" name="meta_content[]" class="form-control" placeholder="meta content/value" value="">
            </div>
            <div class="col-sm-2">
              <button class="btn btn-danger btn-square meta-item-button" type="button" onclick="App.clickCloned(this.id)"><i class="cil-trash"></i></button>
            </div>
          </div>
        <?php else: ?>
          <?php foreach($meta['og']->result as $m => $tag):?>
            <div class="form-group row meta-item">
              <input type="hidden" name="meta_id[]" value="<?php echo $tag['id_meta'] ?>">
              <div class="col-sm-4"><input type="text" name="meta_name[]" class="form-control" placeholder="meta name" value="<?php echo $tag['meta_name'] ?>"></div>
              <div class="col-sm-6">
                <input type="text" name="meta_content[]" class="form-control" placeholder="meta content/value" value="<?php echo $tag['content'] ?>">
              </div>
              <div class="col-sm-2">
                <button class="btn btn-danger btn-square meta-item-button" type="button" onclick="App.clickCloned(this.id)"><i class="cil-trash"></i></button>
              </div>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>
      </div>
      <div class="form-group">
        <button type="button" onclick="App.cloneElement('original-clone', '.meta-list-og')" class="btn btn-square btn-primary"><i class="cil-plus"></i> add meta</button>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-body">
      <div class="form-group">
        <button type="submit" class="btn btn-success btn-square">Save</button>
      </div>
    </div>
  </div>
<?php echo form_close(); ?>