<ul class="nav nav-tabs" id="pageSettingTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link btn-sm active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General Meta</a>
  </li>
  <li class="nav-item">
    <a class="nav-link btn-sm" id="og-tab" data-toggle="tab" href="#og" role="tab" aria-controls="og" aria-selected="false">Open Graph</a>
  </li>
  <li class="nav-item d-none">
    <a class="nav-link btn-sm" id="twittercard-tab" data-toggle="tab" href="#twittercard" role="tab" aria-controls="twittercard" aria-selected="false">Twitter Cards</a>
  </li>
</ul>
<div class="tab-content" id="pageSettingTabContent">
  <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general-tab">
    <?php echo $this->load->view('menu/meta/general', ['id_page' => $page->result['id_page']], TRUE);?>
  </div>
  <div class="tab-pane fade" id="og" role="tabpanel" aria-labelledby="og-tab">
    <?php echo $this->load->view('menu/meta/og', ['id_page' => $page->result['id_page']], TRUE);?>
  </div>
  <div class="tab-pane fade d-none" id="twittercard" role="tabpanel" aria-labelledby="twittercard-tab">
    twitter
  </div>
</div>

<div class="form-group row meta-item d-none" id="original-clone">
  <div class="col-sm-4"><input type="text" name="meta_name[]" class="form-control" placeholder="meta name"></div>
  <div class="col-sm-6">
    <input type="text" name="meta_content[]" class="form-control" placeholder="meta content/value">
  </div>
  <div class="col-sm-2">
    <button class="btn btn-danger btn-square meta-item-button" type="button" onclick="App.clickCloned(this.id)"><i class="cil-trash"></i></button>
  </div>
</div>
