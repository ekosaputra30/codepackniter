<div class="row">
  <div class="col">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link btn-lg <?php echo (!isset($_GET['tab'])) ? 'active' : false ?>" id="banner-tab" data-toggle="tab" href="#banner" role="tab" aria-controls="banner" aria-selected="true">Banner</a>
      </li>
      <li class="nav-item">
        <a class="nav-link btn-lg <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'content') ? 'active' : false ?>" id="contents-tab" data-toggle="tab" href="#contents" role="tab" aria-controls="contents" aria-selected="false">Contents</a>
      </li>
      <li class="nav-item">
        <a class="nav-link btn-lg <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'pagesetting') ? 'active' : false ?>" id="pagesetting-tab" data-toggle="tab" href="#pagesetting" role="tab" aria-controls="pagesetting" aria-selected="false">SEO Meta tag</a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade <?php echo (!isset($_GET['tab'])) ? 'show active' : false ?>" id="banner" role="tabpanel" aria-labelledby="banner-tab">
        <?php echo $this->load->view('menu/banner/index', ['banners' => $page->result['banners']], TRUE);?>
      </div>
      <div class="tab-pane fade <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'content') ? 'show active' : false ?>" id="contents" role="tabpanel" aria-labelledby="contents-tab">
        <?php echo $this->load->view('menu/block/index', ['blocks' => $page->result['blocks']], TRUE);?>
      </div>
      <div class="tab-pane fade <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'pagesetting') ? 'show active' : false ?>" id="pagesetting" role="tabpanel" aria-labelledby="pagesetting-tab">
        <?php echo $this->load->view('menu/meta/index', ['meta' => $meta], TRUE);?>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myNewModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Select Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe width="100%" height="400" src="<?php echo base_url() ?>filemanager/dialog.php?type=2&field_id=new_image&fldr=banner&&akey=cfRAHqgOpv" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Select Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe width="100%" height="400" src="<?php echo base_url() ?>filemanager/dialog.php?type=2&field_id=edit_image&fldr=banner&&akey=cfRAHqgOpv" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
      </div>
    </div>
  </div>
</div>