<div class="row">
	<div class="col-sm-6">
		<div class="card">
			<div class="card-header">
				<a href="<?php echo site_url('menu/'.$menu) ?>" class="btn btn-default"><i class="cil-arrow-left"></i></a>

				<a data-toggle="modal" href="javascript:;" data-target="#myNewModal" class="btn btn-square btn-primary" type="button"><i class="cil-plus"></i> select file</a>
			</div>
			<div class="card-header">

				<?php echo form_open($form_action) ?>
					<input type="hidden" name="id_page" id="id_page" value="<?php echo $page->result['id_page'] ?>">
					<input type="hidden" name="page_name" id="page_name" value="<?php echo $this->_page ?>">
					<input type="hidden" name="new_name" id="new_name" value="banner-<?php echo $menu ?>">
					<input type="hidden" name="new_image" id="new_image" required>

					<div class="row">
						<div class="col-sm-12">
							<img src="" alt="" id="preview_new_image" class="card-img-top">
						</div>
						<div class="col-sm-12">
							<div class="form-group row">
								<label for="heading" class="col-sm-4 col-form-label">date start-end</label>
								<div class="col-sm-8">
									<input type="text" class="form-control datepicker" name="new_datepicker" placeholder="" aria-label="">
								</div>
							</div>
							<div class="form-group row">
								<label for="heading" class="col-sm-4 col-form-label">heading</label>
								<div class="col-sm-8">
									<input type="text" name="new_heading" id="heading" class="form-control" placeholder="heading">
								</div>
							</div>
							<div class="form-group row">
								<label for="sub" class="col-sm-4 col-form-label">sub heading</label>
								<div class="col-sm-8">
									<input type="text" name="new_sub" id="sub" class="form-control" placeholder="sub">
								</div>
							</div>
							<div class="form-group row">
								<label for="content" class="col-sm-4 col-form-label">content</label>
								<div class="col-sm-8">
									<textarea name="new_content" id="content" class="form-control"></textarea>
								</div>
							</div>
							<div class="form-group row">
								<label for="style" class="col-sm-4 col-form-label">style</label>
								<div class="col-sm-8">
									<textarea name="new_style" id="style" class="form-control"></textarea>
								</div>
							</div>
							<div class="form-group row">
								<label for="video" class="col-form-label col-sm-4">is video?</label>
								<div class="col-sm-8">
									<div class="input-group">
										<div class="input-group-prepend">
											<div class="input-group-text">
												<input type="checkbox" name="new_is_video" id="is_video" aria-label="Radio button for following text input">
											</div>
										</div>
										<input type="text" name="new_video" id="video" class="form-control" aria-label="Text input with radio button">
									</div>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-8 offset-sm-4">
									<button type="submit" class="btn btn-success">Submit</button>
									<a href="<?php echo site_url('menu/'.$menu) ?>" class="btn btn-default">cancel</a>
								</div>
							</div>
						</div>
					</div>
				<?php echo form_close()?>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myNewModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Select Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe width="100%" height="400" src="<?php echo base_url() ?>filemanager/dialog.php?type=2&field_id=new_image&fldr=banner&&akey=cfRAHqgOpv" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
      </div>
    </div>
  </div>
</div>
