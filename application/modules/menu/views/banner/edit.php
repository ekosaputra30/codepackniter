<div class="row">
  <div class="col-sm-12">
    <?php if(validation_errors()):?>
      <div class="alert" role="alert">
        <?php echo validation_errors() ?>
      </div>
    <?php endif;?>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <a href="<?php echo site_url('menu/'.$page) ?>" class="btn btn-default"><i class="cil-arrow-left"></i></a>

        <a data-toggle="modal" href="javascript:;" data-target="#myEditModal" class="btn btn-square btn-primary" type="button"><i class="cil-plus"></i> select file</a>
      </div>
      <div class="card-header">

        <?php echo form_open($form_action) ?>
          <input type="hidden" name="id_page" id="id_page" value="<?php echo $result['page'] ?>">
          <input type="hidden" name="page_name" id="page_name" value="<?php echo $page ?>">
          <input type="hidden" name="edit_name" id="edit_name" value="banner-<?php echo $page ?>">
          <input type="hidden" name="edit_image" id="edit_image" value="<?php echo $result['image'] ?>" required>

          <div class="row">
            <div class="col-sm-6 mb-3">
              <img src="<?php echo $result['image'] ?>" alt="" id="edit_image" class="card-img-top">
            </div>
            <div class="col-sm-6">
              <div class="form-group row">
                <label for="heading" class="col-sm-4 col-form-label">date start-end</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control datepicker" name="edit_datepicker" placeholder="" aria-label="" value="<?php echo $result['date_start'].' - '.$result['date_end'] ?>">
                </div>
              </div>
              <div class="form-group row">
                <label for="heading" class="col-sm-4 col-form-label">heading</label>
                <div class="col-sm-8">
                  <input type="text" name="edit_heading" id="heading" class="form-control" placeholder="heading" value="<?php echo $result['heading'] ?>">
                </div>
              </div>
              <div class="form-group row">
                <label for="sub" class="col-sm-4 col-form-label">sub heading</label>
                <div class="col-sm-8">
                  <input type="text" name="edit_sub" id="sub" class="form-control" placeholder="sub" value="<?php echo $result['sub'] ?>">
                </div>
              </div>
              <div class="form-group row">
                <label for="content" class="col-sm-4 col-form-label">content</label>
                <div class="col-sm-8">
                  <textarea name="edit_content" id="content" class="form-control"><?php echo $result['content'] ?></textarea>
                </div>
              </div>
              <div class="form-group row">
                <label for="style" class="col-sm-4 col-form-label">style</label>
                <div class="col-sm-8">
                  <textarea name="edit_style" id="style" class="form-control"><?php echo $result['style'] ?></textarea>
                </div>
              </div>
              <div class="form-group row">
                <label for="video" class="col-form-label col-sm-4">is video?</label>
                <div class="col-sm-8">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <input type="checkbox" name="edit_is_video" id="is_video" aria-label="Radio button for following text input" <?php echo ($result['is_video'] == 1) ? 'checked' : false ?>>
                      </div>
                    </div>
                    <input type="text" name="edit_video" id="video" class="form-control" aria-label="Text input with radio button" value="<?php echo $result['video'] ?>">
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-8 offset-sm-4">
                  <button type="submit" class="btn btn-success">Submit</button>
                  <a href="<?php echo site_url('menu/'.$page) ?>" class="btn btn-default">cancel</a>
                </div>
              </div>
            </div>
          </div>
        <?php echo form_close()?>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myEditModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Select Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <iframe width="100%" height="400" src="<?php echo base_url() ?>filemanager/dialog.php?type=2&field_id=edit_image&fldr=banner&&akey=cfRAHqgOpv" frameborder="0" style="overflow: scroll; overflow-x: hidden; overflow-y: scroll; "></iframe>
      </div>
    </div>
  </div>
</div>
