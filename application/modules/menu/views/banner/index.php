<div class="card">
          <div class="card-header">
            <?php if(count($banners) == 0): ?>
              <a data-toggle="modal" href="javascript:;" data-target="#myNewModal" class="btn btn-square btn-primary" type="button"><i class="cil-plus"></i> add banner</a>
            <?php else:?>
              <a href="<?php echo site_url($this->_module.'/'.$this->_page.'/banner/create') ?>" class="btn btn-square btn-primary"><i class="cil-plus"></i> add banner</a>
            <?php endif;?>
          </div>
          <div class="card-body">
            <?php if(count($banners) == 0): ?>
              <?php echo form_open($form_action_new) ?>
                <input type="hidden" name="id_page" id="id_page" value="<?php echo $page->result['id_page'] ?>">
                <input type="hidden" name="page_name" id="page_name" value="<?php echo $this->_page ?>">
                <input type="hidden" name="new_name" id="new_name" value="banner-<?php echo $this->_page ?>">
                <input type="hidden" name="new_image" id="new_image" required>

                <div class="row">
                  <div class="col-sm-12">
                    <img src="" alt="" id="preview_new_image" class="card-img-top">
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group row">
                      <label for="heading" class="col-sm-4 col-form-label">date start-end</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" name="new_datepicker" placeholder="" aria-label="">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="heading" class="col-sm-4 col-form-label">heading</label>
                      <div class="col-sm-8">
                        <input type="text" name="new_heading" id="heading" class="form-control" placeholder="heading">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="sub" class="col-sm-4 col-form-label">sub heading</label>
                      <div class="col-sm-8">
                        <input type="text" name="new_sub" id="sub" class="form-control" placeholder="sub">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="content" class="col-sm-4 col-form-label">content</label>
                      <div class="col-sm-8">
                        <textarea name="new_content" id="content" class="form-control"></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="style" class="col-sm-4 col-form-label">style</label>
                      <div class="col-sm-8">
                        <textarea name="new_style" id="style" class="form-control"></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="video" class="col-form-label col-sm-4">is video?</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" name="new_is_video" id="is_video" aria-label="Radio button for following text input">
                            </div>
                          </div>
                          <input type="text" name="new_video" id="video" class="form-control" aria-label="Text input with radio button">
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-8 offset-sm-4">
                        <button type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              <?php echo form_close()?>
            <?php endif;?>
            
            <div class="row">
              <?php foreach($banners as $b => $banner): ?>
                <div class="col-sm-4 col-xs-6">
                  <div class="card card-banner text-white">
                    <img class="card-img" src="<?php echo $banner['image'] ?>" alt="Card image cap">
                    <div class="card-img-overlay d-flex flex-column justify-content-center align-items-center">
                      <div class="btn-group" role="group" aria-label="">
                        <a href="<?php echo site_url($this->_module.'/'.$this->_page.'/banner/edit/'.$banner['id']) ?>" class="btn btn-square btn-info"><i class="cil-pencil"></i></a>
                        <a href="<?php echo site_url($this->_module.'/'.$this->_page.'/banner/show/'.$banner['id']) ?>" class="btn btn-square btn-light"><i class="cil-magnifying-glass"></i></a>
                        <a href="<?php echo site_url($this->_module.'/'.$this->_page.'/banner/destroy/'.$banner['id']) ?>" class="btn btn-square btn-danger" onclick="return confirm('Are you sure?')"><i class="cil-trash"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              <?php endforeach;?>
            </div>
          </div>
        </div>