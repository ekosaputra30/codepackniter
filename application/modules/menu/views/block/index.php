        <div class="card">
          <div class="card-header">
            <a href="<?php echo site_url($this->_module.'/'.$this->_page.'/block/create') ?>" class="btn btn-primary btn-square"><i class="cil-plus"></i> add content</a>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th class="border-top-0"></th>
                    <th class="border-top-0">Name</th>
                    <th class="border-top-0">Description</th>
                    <th class="border-top-0">#</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(count($blocks) == 0): ?>
                    <tr>
                      <td colspan="4">no data available</td>
                    </tr>
                  <?php else:?>
                    <?php foreach($blocks as $b => $block):?>
                      <tr>
                        <td scope="row"></td>
                        <td><?php echo $block['name'] ?></td>
                        <td><?php echo $block['description'] ?></td>
                        <td>
                          <div class="btn-group" role="group" aria-label="">
                            <a href="<?php echo site_url($this->_module.'/'.$this->_page.'/block/edit/'.$block['id']) ?>" class="btn btn-square btn-info"><i class="cil-pencil"></i></a>
                            <a href="<?php echo site_url($this->_module.'/'.$this->_page.'/block/show/'.$block['id']) ?>" class="btn btn-square btn-light"><i class="cil-magnifying-glass"></i></a>
                            <a href="<?php echo site_url($this->_module.'/'.$this->_page.'/block/delete/'.$block['id']) ?>" class="btn btn-square btn-danger"><i class="cil-trash"></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php endforeach;?>
                  <?php endif;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>