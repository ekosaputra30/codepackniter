<div class="row">
  <div class="col">
    <a href="<?php echo module_url('?tab=content') ?>" class="btn btn-default btn-square"><i class="cil-arrow-left"></i></a>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4>Meta</h4>
      </div>
      <div class="card-body">
        <div class="form-group row">
          <label for="name" class="col-sm-4 col-form-label">Block Name</label>
          <div class="col-sm-8">
            <input type="text" value="<?php echo (set_value('name')) ? set_value('name') : $block['name'] ?>" name="name" id="name" class="form-control-plaintext" placeholder="" required>
          </div>
        </div>
        <div class="form-group row">
          <label for="description" class="col-sm-4 col-form-label">Short Description</label>
          <div class="col-sm-8">
            <textarea name="description" id="description" class="form-control-plaintext" placeholder=""><?php echo (set_value('description')) ? set_value('description') : $block['description'] ?></textarea>
          </div>
        </div>
        <div class="form-group row">
          <label for="heading" class="col-sm-4 col-form-label">Heading Text</label>
          <div class="col-sm-8">
            <input type="text" value="<?php echo (set_value('heading')) ? set_value('heading') : $block['heading'] ?>" name="heading" id="heading" class="form-control-plaintext"placeholder="">
          </div>
        </div>
        <div class="form-group row">
          <label for="sub" class="col-sm-4 col-form-label">Sub Heading Text</label>
          <div class="col-sm-8">
            <input type="text" value="<?php echo (set_value('sub')) ? set_value('sub') : $block['sub'] ?>" name="sub" id="sub" class="form-control-plaintext"placeholder="">
          </div>
        </div>
        <div class="form-group row">
          <label for="content" class="col-sm-4 col-form-label">Content</label>
          <div class="col-sm-8">
            <textarea name="content" id="content" class="form-control-plaintext" style="height: 300px" placeholder=""><?php echo (set_value('content')) ? set_value('content') : $block['content'] ?></textarea>
          </div>
        </div>
        <div class="form-group row">
          <label for="style" class="col-sm-4 col-form-label">Custom Style</label>
          <div class="col-sm-8">
            <textarea name="style" id="style" class="form-control-plaintext" placeholder=""><?php echo (set_value('style')) ? set_value('style') : $block['style'] ?></textarea>
          </div>
        </div>
        <div class="form-group row">
          <label for="link" class="col-sm-4 col-form-label">link</label>
          <div class="col-sm-8">
            <textarea name="link" id="link" class="form-control-plaintext" placeholder=""><?php echo (set_value('link')) ? set_value('link') : $block['link'] ?></textarea>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-12 col-sm-6">
    <div class="card">
      <div class="card-header">
        <h4>Image</h4>
      </div>
      <div class="card-body">
        <div class="form-group row <?php echo !empty($block['image']) ? false : 'd-none' ?>" id="preview_image_block">
          <label for="image" class="col-sm-4 col-form-label">Preview</label>
          <div class="col-sm-8">
            <img src="<?php echo $block['image'] ?>" id="preview_image" alt="" class="img-thumbnail">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>