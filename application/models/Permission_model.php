<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Permission_model extends CI_Model {
  private $_table = 'permissions';

  public function get($id = false) {
    $return = new ArrayObject();
    $query = $this->db->get_where($this->_table, ['perm_id' => $id]);

    $return->count  = $query->num_rows();
    $return->result = $query->row_array();

    return $return;
  }

  public function getPermissions() {
    $return = $this->db->get($this->_table);
    return $return->result_array();
  }

  public function save($data) {
    if ($this->db->insert($this->_table, $data)) return $this->db->insert_id();

    else return false;
  }

  public function edit($id, $data) {
    if ($this->db->update($this->_table, $data, ['perm_id' => $id])) return true;

    else return false;
  }

  public function delete($id) {
    if ($this->db->delete($this->_table, ['perm_id' => $id])) return true;
    
    return false;
  }

}

/* End of file Permission_model.php */
