<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends MX_Controller {
  public $_module;
  public $_page;
  public $_current;
  public $_menu_slug;
  
  public function __construct()
  {
    parent::__construct();

    $this->load->library('Dash');
    $this->dash->is_secure();
    $this->load->library('Auth');
  }

	public function _render($data = array()) {
		return $this->load->view('admin_master', $data);
  }
  
  public function set_fdata($name = false, $value = false) {
    return $this->session->set_flashdata($name, $value);
  }

  public function get_fdata($name) {
    return $this->session->flashdata($name);
  }
}

/* End of file Landing.php */
