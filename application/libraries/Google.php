<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'vendor/google/apiclient/src/Google/Client.php';

class Google extends Google_Client
{
  protected $ci;

  public function __construct($params = array())
  {
    $this->ci =& get_instance();
  }
}

/* End of file Google.php */
