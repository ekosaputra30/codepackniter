<?php
class Dash {
  public $_user;
  private $CI;

  public function __construct()
  {
    $this->CI =& get_instance();
  }
  
  public function is_secure() {
    $userdata = $this->CI->session->has_userdata('cms_data');
    if($userdata) return true;
    return redirect(site_url('login'));
  }

  public function getUser($field = false) {
    $userdata = $this->CI->session->userdata('cms_data');
    return ($field) ? $userdata[$field] : $userdata;
  }
}
