<?php

function admin_url($slug = false) {
  return base_url('/admin/'.$slug);
}

function admin_url_assets($url = false) {
  return base_url('/assets/secure/'.$url);
}

function module_url($slug = false, $module = false, $page = false) {
  $CI     =& get_instance();
  $module =  $CI->uri->segment(1);
  $page   =  $CI->uri->segment(2);
  
  return site_url($module.'/'.$page.'/'.$slug);
}

function get_pages() {
  $CI     =& get_instance();
  $CI->load->model('settings/Page_model');
  $pages = $CI->Page_model->get_all(TRUE, 'ASC');
  
  return $pages;
}