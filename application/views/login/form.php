<!doctype html>
<html lang="en">
  <head>
    <title>Administrator</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="<?php echo admin_url_assets('/css//coreui.min.css') ?>">
  </head>
  <body class="c-app flex-row align-items-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-4">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <h1>Login</h1>
                <p class="text-muted">Sign In to your account</p>
                <div class="alert alert-danger <?php echo ($form_login_cb) ? false : 'd-none' ?>" role="alert">
                  <strong><?php echo $form_login_cb ?></strong>
                </div>
                <?php echo form_open($form_action) ?>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend"><span class="input-group-text">
                      <svg class="c-icon">
                        <use xlink:href="<?php echo admin_url_assets() ?>/vendors/@coreui/icons/svg/free.svg#cil-user"></use>
                      </svg></span>
                    </div>
                    <input class="form-control" name="email" type="text" placeholder="email">
                  </div>
                  <div class="input-group mb-4">
                    <div class="input-group-prepend"><span class="input-group-text">
                      <svg class="c-icon">
                        <use xlink:href="<?php echo admin_url_assets() ?>/vendors/@coreui/icons/svg/free.svg#cil-lock-locked"></use>
                      </svg></span>
                    </div>
                    <input class="form-control" name="password" type="password" placeholder="Password">
                  </div>
                  <div class="row">
                    <div class="col-6">
                      <button class="btn btn-primary px-4" type="submit">Login</button>
                    </div>
                    <div class="col-6 text-right">
                      <button class="btn btn-link px-0" type="button">Forgot password?</button>
                    </div>
                  </div>
                <?php echo form_close() ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="<?php echo admin_url_assets('/js/coreui.bundle.min.js') ?>"></script>
  </body>
</html>