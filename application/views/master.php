<!doctype html>
<html lang="en">
  <head>
    <title>Codepackniter</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="<?php echo codepackniter('css/app.css') ?>">
  </head>
  <body>
    <?php echo $this->load->view($page_view, $page_data, TRUE) ?>

    <script src="<?php echo codepackniter('js/app.js') ?>"></script>
  </body>
</html>