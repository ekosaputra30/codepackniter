<?php 

$str = '
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link rel="stylesheet" href="'.codepackniter('secure/css/app.css').'">
</head>
<body>
  <div class="container">
    <h3>unit test</h3>
    <table class="table" border="0" cellpadding="4" cellspacing="1">
    {rows}
    <tr>
    <td>{item}</td>
    <td>{result}</td>
    </tr>
    {/rows}
    </table>
  </div>
</body>
</html>
';

$this->unit->set_template($str);

echo $this->unit->run($test, $expect, $name);
?>