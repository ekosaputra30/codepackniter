<!doctype html>
<html lang="en">
  <head>
    <title>Administrator</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="<?php echo admin_url_assets('/css//coreui.min.css') ?>">
    <!-- <link rel="stylesheet" href="https://unpkg.com/@coreui/icons@1.0.0-beta.0/css/all.min.css"> -->
    <link rel="stylesheet" href="<?php echo admin_url_assets('/vendors/@coreui/icons/css/free.min.css') ?>">
    <link rel="stylesheet" href="<?php echo admin_url_assets('/vendors/@coreui/icons/css/brand.min.css') ?>">
    <link rel="stylesheet" href="<?php echo admin_url_assets('/vendors/@coreui/icons/css/flag.min.css') ?>">
    <link rel="stylesheet" href="<?php echo codepackniter('secure/css/app.css') ?>">
    <script src="https://apis.google.com/js/api.js"></script>
    <script>
      (function(w,d,s,g,js,fs){
        g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
        js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
        js.src='https://apis.google.com/js/platform.js';
        fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
      }(window,document,'script'));

      var App = null
      var base_url = "<?php echo site_url() ?>";
      var Alert = {
        success: {
          is_valid: "<?php echo ($this->session->flashdata('cb:form:success')) ? true : false;?>",
          message: "<?php echo $this->session->flashdata('cb:form:success');?>"
        },
        alert: {
          is_valid: "<?php echo ($this->session->flashdata('cb:form:alert')) ? true : false;?>",
          message: "<?php echo $this->session->flashdata('cb:form:alert');?>"
        },
        danger: {
          is_valid: "<?php echo ($this->session->flashdata('cb:form:danger')) ? true : false;?>",
          message: "<?php echo $this->session->flashdata('cb:form:danger');?>"
        },
        info: {
          is_valid: "<?php echo ($this->session->flashdata('cb:form:info')) ? true : false;?>",
          message: "<?php echo $this->session->flashdata('cb:form:info');?>"
        },
      }

      function responsive_filemanager_callback(field_id){
        let url = jQuery('#'+field_id).val();
        let preview_img = $('#preview_'+field_id)
        let preview_block = $('#preview_'+field_id+'_block')

        preview_block.removeClass('d-none');
        preview_img.attr('src', url)
      }
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    </html>
  </head>
  <body class="c-app">
    <div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
      <div class="c-sidebar-brand"><img class="c-sidebar-brand-full" src="<?php echo admin_url_assets() ?>/assets/brand/coreui-base-white.svg" width="118" height="46" alt="CoreUI Logo"><img class="c-sidebar-brand-minimized" src="<?php echo admin_url_assets() ?>/assets/brand/coreui-signet-white.svg" width="118" height="46" alt="CoreUI Logo"></div>
      <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-title">Menu</li>
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?php echo ($this->_page == 'admin') ? 'c-active' : false; ?>" href="<?php echo admin_url('') ?>">
          <svg class="c-sidebar-nav-icon">
            <use xlink:href="<?php echo admin_url_assets(); ?>/vendors/@coreui/icons/svg/free.svg#cil-speedometer"></use>
          </svg> Dashboard</a>
        </li>

        <?php if(isset($this->dash->getUser('permissions')['basic_crud'])): ?>
          <?php foreach(get_pages()->result as $key_p => $page):?>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?php echo ($this->_page == $page['module_name']) ? 'c-active' : false; ?>" href="<?php echo site_url('menu/'.$page['slug']) ?>">
              <div class="c-sidebar-nav-icon">
                <i class="cil-apps"></i>
              </div> <?php echo $page['page_name'] ?></a>
            </li>
          <?php endforeach ?>
        <?php endif;?>

        <?php if(isset($this->dash->getUser('permissions')['cms_setting'])): ?>
          <li class="c-sidebar-nav-title">Setting</li>
          <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?php echo ($this->_page == 'settings') ? 'c-active' : false; ?>" href="<?php echo base_url('settings') ?>">
            <div class="c-sidebar-nav-icon">
              <i class="cib-pagekit"></i> 
            </div> General Setting</a>
          </li>
          <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?php echo ($this->_page == 'page') ? 'c-active' : false; ?>" href="<?php echo base_url('settings/page') ?>">
            <div class="c-sidebar-nav-icon">
              <i class="cib-pagekit"></i> 
            </div> Pages</a>
          </li>
          <li class="c-sidebar-nav-item d-none"><a class="c-sidebar-nav-link <?php echo ($this->_page == 'banner') ? 'c-active' : false; ?>" href="<?php echo base_url('settings/banner') ?>">
            <div class="c-sidebar-nav-icon">
              <i class="cib-pagekit"></i> 
            </div> Banners</a>
          </li>
          <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?php echo ($this->_page == 'file') ? 'c-active' : false; ?>" href="<?php echo base_url('settings/filemanager') ?>">
            <div class="c-sidebar-nav-icon">
              <i class="cib-pagekit"></i> 
            </div> File Manager</a>
          </li>
        <?php endif;?>

        <?php if(isset($this->dash->getUser('permissions')['rbac'])): ?>
          <li class="c-sidebar-nav-title">RBAC</li>
          <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?php echo ($this->_page == 'user') ? 'c-active' : false; ?>" href="<?php echo base_url('rbac/user') ?>">
            <div class="c-sidebar-nav-icon">
              <i class="cib-pagekit"></i> 
            </div> Users</a>
          </li>
          <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link <?php echo ($this->_page == 'role') ? 'c-active' : false; ?>" href="<?php echo base_url('rbac/role') ?>">
            <div class="c-sidebar-nav-icon">
              <i class="cib-pagekit"></i> 
            </div> Roles</a>
          </li>
        <?php endif;?>
      </ul>
      <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
    </div>
    <div class="c-wrapper">
      <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
        <button class="c-header-toggler c-class-toggler d-lg-none mr-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show"><span class="c-header-toggler-icon"></span></button><a class="c-header-brand d-sm-none" href="#"><img class="c-header-brand" src="<?php echo admin_url_assets() ?>/assets/brand/coreui-base.svg" width="97" height="46" alt="CoreUI Logo"></a>
        <button class="c-header-toggler c-class-toggler ml-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true"><span class="c-header-toggler-icon"></span></button>
        <ul class="c-header-nav ml-auto mr-4">
          <li class="c-header-nav-item dropdown"><a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <div class="c-avatar"><img class="c-avatar-img" src="<?php echo admin_url_assets() ?>/assets/img/avatars/6.jpg" alt="user@email.com"></div>
            </a>
            <div class="dropdown-menu dropdown-menu-right pt-0">
              <div class="dropdown-header bg-light py-2"><strong>Settings</strong></div>
              <a class="dropdown-item" href="<?php echo admin_url('settings/profile') ?>">
                <svg class="c-icon mr-2">
                  <use xlink:href="<?php echo admin_url_assets(); ?>/vendors/@coreui/icons/svg/free.svg#cil-user"></use>
                </svg> Profile
              </a>
              <a class="dropdown-item" href="<?php echo admin_url('settings/page') ?>">
                <svg class="c-icon mr-2">
                  <use xlink:href="<?php echo admin_url_assets(); ?>/vendors/@coreui/icons/svg/free.svg#cib-pagekit"></use>
                </svg> Pages
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="<?php echo site_url('logout') ?>">
                <svg class="c-icon mr-2">
                  <use xlink:href="<?php echo admin_url_assets(); ?>/vendors/@coreui/icons/svg/free.svg#cil-account-logout"></use>
                </svg> Logout
              </a>
            </div>
          </li>
        </ul>
        <div class="c-subheader px-3">
          <?php echo $this->load->view('admin/breadcrumb', [], TRUE);?>
        </div>
      </header>
      <div class="c-body">
        <main class="c-main">
          <div class="container-fluid">
            <div class="fade-in">
              <?php echo $this->load->view($page_view, $page_data, TRUE) ?>
            </div>
          </div>
        </main>
      </div>
      <footer class="c-footer">
        <div><a href="https://coreui.io">CoreUI</a> © 2019 creativeLabs.</div>
        <div class="ml-auto">Powered by&nbsp;<a href="https://coreui.io/">CoreUI</a></div>
      </footer>
    </div>

    <script src="<?php echo codepackniter('secure/js/gapi-components.js') ?>"></script>
    <script src="<?php echo codepackniter('secure/js/app.js') ?>"></script>
    <script src="<?php echo admin_url_assets('/js/coreui.bundle.min.js') ?>"></script>
  </body>
</html>